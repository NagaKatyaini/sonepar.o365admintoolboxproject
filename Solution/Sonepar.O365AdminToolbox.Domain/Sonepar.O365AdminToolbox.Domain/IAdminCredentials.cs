﻿namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IAdminCredentials
    {
        UserPassCredentials GetAdminCredentials();
    }
     //"admin@MOD933703.onmicrosoft.com"
}
