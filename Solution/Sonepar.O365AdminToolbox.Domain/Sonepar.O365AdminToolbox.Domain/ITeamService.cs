﻿using Sonepar.O365AdminToolbox.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface ITeamService
    {
        Task<IEnumerable<OfficeObject>> GetTeamsManagedByUser(IEnumerable<string> countries);
        Task<OfficeObject> GetTeam(string id, IEnumerable<string> countries);

        Task<OfficeObject> NewTeam(OfficeObject team, IEnumerable<string> countries);

        Task<OfficeObject> UpdateTeam(OfficeObject team, IEnumerable<string> countries);
        Task DeleteTeam(string id, IEnumerable<string> countries);

        Task<IEnumerable<Member>> GetTeamOwners(string id, IEnumerable<string> countries);
        Task AddTeamOwner(string id, string userId, IEnumerable<string> countries);
        Task RemoveTeamOwner(string id, string userId, IEnumerable<string> countries);

        Task<IEnumerable<Member>> GetTeamMembers(string id, IEnumerable<string> countries);
        Task AddTeamMember(string id, string userId, IEnumerable<string> countries);
        Task RemoveTeamMember(string id, string userId, IEnumerable<string> countries);
    }
}