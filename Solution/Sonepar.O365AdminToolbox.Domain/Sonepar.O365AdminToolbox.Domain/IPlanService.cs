﻿using Sonepar.O365AdminToolbox.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IPlanService
    {
        Task<IEnumerable<OfficeObject>> GetPlansManagedByUser(IEnumerable<string> countries);
        Task<OfficeObject> GetPlanAsync(string id, IEnumerable<string> countries);

        Task<OfficeObject> NewPlan(OfficeObject plan, IEnumerable<string> countries);
        Task<OfficeObject> UpdatePlan(OfficeObject team, IEnumerable<string> countries);

        Task DeletePlan(string plan, IEnumerable<string> countries);

        Task<IEnumerable<Member>> GetPlanOwners(string id, IEnumerable<string> countries);
        Task AddPlanOwner(string id, string userId, IEnumerable<string> countries);
        Task RemovePlanOwner(string id, string userId, IEnumerable<string> countries);

        Task<IEnumerable<Member>> GetPlanMembers(string id, IEnumerable<string> countries);
        Task AddPlanMember(string id, string userId, IEnumerable<string> countries);
        Task RemovePlanMember(string id, string userId, IEnumerable<string> countries);
    }
}