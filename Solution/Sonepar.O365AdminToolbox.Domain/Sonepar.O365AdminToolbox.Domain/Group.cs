﻿using Sonepar.O365AdminToolbox.Entity;
using System.Collections.Generic;

namespace Sonepar.O365AdminToolbox.Domain
{
    /// <summary>
    /// A office 365 group 
    /// </summary>
    /// <seealso cref="Member" />
    public class Group : Member
    {
        /// <summary>
        /// Gets or sets the tenant country.
        /// </summary>
        /// <value>
        /// The tenant country.
        /// </value>
        public string Country { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the is subscribed by mail.
        /// </summary>
        /// <value>
        /// The is subscribed by mail (readonly)
        /// </value>
        public bool? IsSubscribedByMail { get; set; }
        /// <summary>
        /// Gets or sets the mail.
        /// </summary>
        /// <value>
        /// The mail.
        /// </value>
        public string Mail { get; set; }
        /// <summary>
        /// Gets or sets the mail nickname.
        /// </summary>
        /// <value>
        /// The mail nickname.
        /// </value>
        public string MailNickname { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the owners.
        /// </summary>
        /// <value>
        /// The owners (used fro creation only).
        /// </value>
        public IEnumerable<Member> Owners { get; set; }
        /// <summary>
        /// Gets or sets the security enabled.
        /// </summary>
        /// <value>
        /// The security enabled (read-only)
        /// </value>
        public bool? SecurityEnabled { get; set; }
        /// <summary>
        /// Gets or sets the visibility.
        /// </summary>
        /// <value>
        /// The visibility : Public or Private
        /// </value>
        public string Visibility { get; set; }
    }
}
