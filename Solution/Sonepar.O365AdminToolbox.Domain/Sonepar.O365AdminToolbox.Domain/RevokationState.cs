﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Domain
{
    public class RevokationState
    {
        public bool IsRevoked { get; set; }

        public bool IsBloqued { get; set; }

        public bool IsDeconnectedFromSPO { get; set; }

        public string Message { get; set; }

    }
}
