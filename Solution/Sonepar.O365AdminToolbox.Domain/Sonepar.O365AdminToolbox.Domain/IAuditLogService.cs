﻿namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IAuditLogService
    {
        void LogAction(string userId, string data, string action);

    }
}
