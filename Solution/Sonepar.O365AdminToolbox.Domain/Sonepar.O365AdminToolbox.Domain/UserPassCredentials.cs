﻿using System.Security;

namespace Sonepar.O365AdminToolbox.Domain
{
    public class UserPassCredentials
    {
        public string UserLogin { get; set; }
        public SecureString Password { get; set;}
    }
}
