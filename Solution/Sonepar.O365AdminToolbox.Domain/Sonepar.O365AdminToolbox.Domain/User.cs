﻿using Sonepar.O365AdminToolbox.Entity;
using System;
using System.Collections.Generic;

namespace Sonepar.O365AdminToolbox.Domain
{
    /// <summary>
    /// a User
    /// </summary>
    /// <seealso cref="Member" />
    public class User : Member
    {
        /// <summary>
        /// Gets or sets the account enabled.
        /// </summary>
        /// <value>
        /// The account enabled.
        /// </value>
        public bool? AccountEnabled { get; set; }
        /// <summary>
        /// Gets or sets the assigned licenses.
        /// </summary>
        /// <value>
        /// The assigned licenses.
        /// </value>
        public IList<License> AssignedLicenses { get; set; }
        /// <summary>
        /// Gets or sets the assigned plans (products).
        /// </summary>
        /// <value>
        /// The assigned plans (products).
        /// </value>
        public IList<Service> AssignedPlans { get; set; }
        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        public string CompanyName { get; set; }
        /// <summary>
        /// Gets or sets the country (location).
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country { get; set; }
        //public string CreationType { get; set; }
        /// <summary>
        /// Gets or sets the department.
        /// </summary>
        /// <value>
        /// The department.
        /// </value>
        public string Department { get; set; }
        /// <summary>
        /// Gets or sets the on premises synchronize enabled.
        /// </summary>
        /// <value>
        /// The on premises synchronize enabled.
        /// </value>
        public bool? OnPremisesSyncEnabled { get; set; }
        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>
        /// The display name.
        /// </value>
        public string DisplayName { get; set; }
        /// <summary>
        /// Gets or sets the business phones.
        /// </summary>
        /// <value>
        /// The business phones.
        /// </value>
        public IEnumerable<string> BusinessPhones { get; set; }
        /// <summary>
        /// Gets or sets the name of the given.
        /// </summary>
        /// <value>
        /// The name of the given.
        /// </value>
        public string GivenName { get; set; }
        /// <summary>
        /// Gets or sets the job title.
        /// </summary>
        /// <value>
        /// The job title.
        /// </value>
        public string JobTitle { get; set; }
        /// <summary>
        /// Gets or sets the on premises last synchronize date time.
        /// </summary>
        /// <value>
        /// The on premises last synchronize date time.
        /// </value>
        public DateTimeOffset? OnPremisesLastSyncDateTime { get; set; }
        /// <summary>
        /// Gets or sets the mail.
        /// </summary>
        /// <value>
        /// The mail.
        /// </value>
        public string Mail { get; set; }
        /// <summary>
        /// Gets or sets the mail nickname.
        /// </summary>
        /// <value>
        /// The mail nickname.
        /// </value>
        public string MailNickname { get; set; }
        /// <summary>
        /// Gets or sets the mobile phone.
        /// </summary>
        /// <value>
        /// The mobile phone.
        /// </value>
        public string MobilePhone { get; set; }
        /// <summary>
        /// Gets or sets the on premises security identifier.
        /// </summary>
        /// <value>
        /// The on premises security identifier.
        /// </value>
        public string OnPremisesSecurityIdentifier { get; set; }
        /// <summary>
        /// Gets or sets the password policies.
        /// </summary>
        /// <value>
        /// The password policies.
        /// </value>
        public string PasswordPolicies { get; set; }
        /// <summary>
        /// Gets or sets the office location.
        /// </summary>
        /// <value>
        /// The office location.
        /// </value>
        public string OfficeLocation { get; set; }
        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>
        /// The postal code.
        /// </value>
        public string PostalCode { get; set; }
        /// <summary>
        /// Gets or sets the preferred language.
        /// </summary>
        /// <value>
        /// The preferred language.
        /// </value>
        public string PreferredLanguage { get; set; }
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public string State { get; set; }
        /// <summary>
        /// Gets or sets the street address.
        /// </summary>
        /// <value>
        /// The street address.
        /// </value>
        public string StreetAddress { get; set; }
        public string Surname { get; set; }
        /// <summary>
        /// Gets or sets the usage location.
        /// </summary>
        /// <value>
        /// The usage location.
        /// </value>
        public string UsageLocation { get; set; }
        public string UserPrincipalName { get; set; }
        /// <summary>
        /// Gets or sets the type of the user.
        /// </summary>
        /// <value>
        /// The type of the user.
        /// </value>
        public string UserType { get; set; }

        /// <summary>
        /// Gets or sets the tenant countries.
        /// </summary>
        /// <value>
        /// The tenant countries.
        /// </value>
        public IEnumerable<string> TenantCountries { get; set; }
    }
}
