﻿using Sonepar.O365AdminToolbox.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IGroupService
    {
        Task<IEnumerable<Group>> GetGroupsManagedByUser(IEnumerable<string> countries);

        Task<List<Microsoft.Graph.Group>> GetUnregisteredGroups();
        Task<bool> GroupExists(string groupName);
        Task<Group> GetGroup(string id, IEnumerable<string> countries);

        Task<Group> NewGroup(Group g, IEnumerable<string> countries);

        Task<Group> UpdateGroup(Group group, IEnumerable<string> countries);

        Task DeleteGroup(string groupId, IEnumerable<string> countries);

        Task<IEnumerable<Member>> GetGroupOwners(string id, IEnumerable<string> countries);
        Task AddGroupOwner(string groupId, string ownerId, IEnumerable<string> allowedCountries);
        Task RemoveGroupOwner(string groupId, string ownerId, IEnumerable<string> allowedCountries);

        Task<IEnumerable<Member>> GetGroupMembers(string id, IEnumerable<string> countries);
        Task AddGroupMember(string groupId, string id, IEnumerable<string> countries);
        Task RemoveGroupMember(string groupId, string userId, IEnumerable<string> countries);

        Group MapGroup(OfficeObject o);
        OfficeObject MapOfficeObject(Group g);
    }
}