﻿using Sonepar.O365AdminToolbox.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Domain
{
    public interface IFriendlyNameService
    {
        string GetNameOrDefault(string defaultValue, string serviceName, string languageCode);
        // void Register(string registeredName, string serviceName, ServiceType Type);

        // Task<IEnumerable<LicencesNames>> GetLicencesFriendlyNames(string language);

        Task<IEnumerable<LicencesNames>> GetLicencesFriendlyNames();

        Task<bool> UpdateTranslations(List<LicencesNames> licenceNameUpdates);
    }
}
