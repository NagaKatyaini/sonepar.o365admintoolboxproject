﻿using Microsoft.Azure.ActiveDirectory.GraphClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sonepar.O365AdminToolbox.Business
{
    public class UserBusiness
    {
        private IActiveDirectoryClient client;

        public UserBusiness(IActiveDirectoryClient client ) {
            this.client = client;
        }

        public async Task<IEnumerable<IUser>> SearchUser(string perimeter, string searchString) {
            IUserCollection userCollection = client.Users;
            IGroupCollection groupCollection = client.Groups;

            var searchResults = await userCollection.Where(user =>
                user.UserPrincipalName.StartsWith(searchString) ||
                user.DisplayName.StartsWith(searchString) ||
                user.GivenName.StartsWith(searchString) ||
                user.Surname.StartsWith(searchString)).ExecuteAsync();
            var usersList = searchResults.CurrentPage.ToList();
            return usersList;
        }

    }
}
