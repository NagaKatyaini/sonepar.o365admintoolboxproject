namespace Sonepar.O365AdminToolbox.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LicencesNames",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LicenceGuid = c.String(nullable: false, maxLength: 128),
                        FriendlyName = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.OfficeObjects", "Url", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OfficeObjects", "Url");
            DropTable("dbo.LicencesNames");
        }
    }
}
