var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "angular", "./ComponentDecorator", "./Model/Workspace"], function (require, exports, app_1, angular, ComponentDecorator_1, Workspace_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var WorkspacesComponent = /** @class */ (function () {
        function WorkspacesComponent($exceptionHandler, scroll, modal, auth, dataService, logService, notify, notifyOkConfig, config) {
            var _this = this;
            this.$exceptionHandler = $exceptionHandler;
            this.scroll = scroll;
            this.modal = modal;
            this.auth = auth;
            this.dataService = dataService;
            this.logService = logService;
            this.notify = notify;
            this.notifyOkConfig = notifyOkConfig;
            this.config = config;
            this.ownerPlaceHolder = 'Loading users...';
            this.saving = false;
            this.loading = false;
            this.isCreateMode = false;
            this.isEditMode = false;
            this.isOwnersMode = false;
            this.languages = [
                {
                    "value": "German",
                    "key": 1031
                },
                {
                    "value": "French",
                    "key": 1036
                },
                {
                    "value": "Irish",
                    "key": 2108
                },
                {
                    "value": "Indonesian",
                    "key": 1057
                },
                {
                    "value": "Norwegian (Bokmål)",
                    "key": 1044
                },
                {
                    "value": "Russian",
                    "key": 1049
                },
                {
                    "value": "Chinese (Simplified)",
                    "key": 2052
                },
                {
                    "value": "Chinese (Traditional)",
                    "key": 1028
                },
                {
                    "value": "Hindi",
                    "key": 1081
                },
                {
                    "value": "Malay",
                    "key": 1086
                },
                {
                    "value": "Slovenian",
                    "key": 1060
                },
                {
                    "value": "Danish",
                    "key": 1030
                },
                {
                    "value": "Basque",
                    "key": 1069
                },
                {
                    "value": "Finnish",
                    "key": 1035
                },
                {
                    "value": "Dutch",
                    "key": 1043
                },
                {
                    "value": "Slovak",
                    "key": 1051
                },
                {
                    "value": "Azerbaijani",
                    "key": 1068
                },
                {
                    "value": "Bulgarian",
                    "key": 1026
                },
                {
                    "value": "Galician",
                    "key": 1110
                },
                {
                    "value": "Turkish",
                    "key": 1055
                },
                {
                    "value": "Welsh",
                    "key": 1106
                },
                {
                    "value": "Croatian",
                    "key": 1050
                },
                {
                    "value": "Hungarian",
                    "key": 1038
                },
                {
                    "value": "Korean",
                    "key": 1042
                },
                {
                    "value": "Lithuanian",
                    "key": 1063
                },
                {
                    "value": "Macedonian",
                    "key": 1071
                },
                {
                    "value": "Arabic",
                    "key": 1025
                },
                {
                    "value": "Japanese",
                    "key": 1041
                },
                {
                    "value": "Latvian",
                    "key": 1062
                },
                {
                    "value": "Dari",
                    "key": 1164
                },
                {
                    "value": "Portuguese (Brazil)",
                    "key": 1046
                },
                {
                    "value": "Portuguese (Portugal)",
                    "key": 2070
                },
                {
                    "value": "Serbian (Latin, Serbia)",
                    "key": 9242
                },
                {
                    "value": "Thai",
                    "key": 1054
                },
                {
                    "value": "Bosnian (Latin)",
                    "key": 5146
                },
                {
                    "value": "Czech",
                    "key": 1029
                },
                {
                    "value": "Spanish",
                    "key": 3082
                },
                {
                    "value": "Hebrew",
                    "key": 1037
                },
                {
                    "value": "Polish",
                    "key": 1045
                },
                {
                    "value": "Serbian (Cyrillic, Serbia)",
                    "key": 10266
                },
                {
                    "value": "Serbian (Latin)",
                    "key": 2074
                },
                {
                    "value": "Ukrainian",
                    "key": 1058
                },
                {
                    "value": "Greek",
                    "key": 1032
                },
                {
                    "value": "English",
                    "key": 1033
                },
                {
                    "value": "Estonian",
                    "key": 1061
                },
                {
                    "value": "Italian",
                    "key": 1040
                },
                {
                    "value": "Kazakh",
                    "key": 1087
                },
                {
                    "value": "Swedish",
                    "key": 1053
                },
                {
                    "value": "Vietnamese",
                    "key": 1066
                },
                {
                    "value": "Catalan",
                    "key": 1027
                },
                {
                    "value": "Romanian",
                    "key": 1048
                }
            ];
            this.timeZoneList = [
                {
                    "Id": 39,
                    "Description": "(UTC-12:00) International Date Line West"
                },
                {
                    "Id": 95,
                    "Description": "(UTC-11:00) Coordinated Universal Time-11"
                },
                {
                    "Id": 15,
                    "Description": "(UTC-10:00) Hawaii"
                },
                {
                    "Id": 14,
                    "Description": "(UTC-09:00) Alaska"
                },
                {
                    "Id": 78,
                    "Description": "(UTC-08:00) Baja California"
                },
                {
                    "Id": 13,
                    "Description": "(UTC-08:00) Pacific Time (US and Canada)"
                },
                {
                    "Id": 38,
                    "Description": "(UTC-07:00) Arizona"
                },
                {
                    "Id": 77,
                    "Description": "(UTC-07:00) Chihuahua, La Paz, Mazatlan"
                },
                {
                    "Id": 12,
                    "Description": "(UTC-07:00) Mountain Time (US and Canada)"
                },
                {
                    "Id": 55,
                    "Description": "(UTC-06:00) Central America"
                },
                {
                    "Id": 11,
                    "Description": "(UTC-06:00) Central Time (US and Canada)"
                },
                {
                    "Id": 37,
                    "Description": "(UTC-06:00) Guadalajara, Mexico City, Monterrey"
                },
                {
                    "Id": 36,
                    "Description": "(UTC-06:00) Saskatchewan"
                },
                {
                    "Id": 35,
                    "Description": "(UTC-05:00) Bogota, Lima, Quito"
                },
                {
                    "Id": 10,
                    "Description": "(UTC-05:00) Eastern Time (US and Canada)"
                },
                {
                    "Id": 34,
                    "Description": "(UTC-05:00) Indiana (East)"
                },
                {
                    "Id": 88,
                    "Description": "(UTC-04:30) Caracas"
                },
                {
                    "Id": 91,
                    "Description": "(UTC-04:00) Asuncion"
                },
                {
                    "Id": 9,
                    "Description": "(UTC-04:00) Atlantic Time (Canada)"
                },
                {
                    "Id": 81,
                    "Description": "(UTC-04:00) Cuiaba"
                },
                {
                    "Id": 33,
                    "Description": "(UTC-04:00) Georgetown, La Paz, Manaus, San Juan"
                },
                {
                    "Id": 65,
                    "Description": "(UTC-04:00) Santiago"
                },
                {
                    "Id": 28,
                    "Description": "(UTC-03:30) Newfoundland"
                },
                {
                    "Id": 8,
                    "Description": "(UTC-03:00) Brasilia"
                },
                {
                    "Id": 85,
                    "Description": "(UTC-03:00) Buenos Aires"
                },
                {
                    "Id": 32,
                    "Description": "(UTC-03:00) Cayenne, Fortaleza"
                },
                {
                    "Id": 60,
                    "Description": "(UTC-03:00) Greenland"
                },
                {
                    "Id": 90,
                    "Description": "(UTC-03:00) Montevideo"
                },
                {
                    "Id": 103,
                    "Description": "(UTC-03:00) Salvador"
                },
                {
                    "Id": 96,
                    "Description": "(UTC-02:00) Coordinated Universal Time-02"
                },
                {
                    "Id": 30,
                    "Description": "(UTC-02:00) Mid-Atlantic"
                },
                {
                    "Id": 29,
                    "Description": "(UTC-01:00) Azores"
                },
                {
                    "Id": 53,
                    "Description": "(UTC-01:00) Cabo Verde"
                },
                {
                    "Id": 86,
                    "Description": "(UTC) Casablanca"
                },
                {
                    "Id": 93,
                    "Description": "(UTC) Coordinated Universal Time"
                },
                {
                    "Id": 2,
                    "Description": "(UTC) Dublin, Edinburgh, Lisbon, London"
                },
                {
                    "Id": 31,
                    "Description": "(UTC) Monrovia, Reykjavik"
                },
                {
                    "Id": 4,
                    "Description": "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna"
                },
                {
                    "Id": 6,
                    "Description": "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague"
                },
                {
                    "Id": 3,
                    "Description": "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris"
                },
                {
                    "Id": 57,
                    "Description": "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb"
                },
                {
                    "Id": 69,
                    "Description": "(UTC+01:00) West Central Africa"
                },
                {
                    "Id": 83,
                    "Description": "(UTC+01:00) Windhoek"
                },
                {
                    "Id": 79,
                    "Description": "(UTC+02:00) Amman"
                },
                {
                    "Id": 5,
                    "Description": "(UTC+02:00) Athens, Bucharest"
                },
                {
                    "Id": 80,
                    "Description": "(UTC+02:00) Beirut"
                },
                {
                    "Id": 49,
                    "Description": "(UTC+02:00) Cairo"
                },
                {
                    "Id": 98,
                    "Description": "(UTC+02:00) Damascus"
                },
                {
                    "Id": 50,
                    "Description": "(UTC+02:00) Harare, Pretoria"
                },
                {
                    "Id": 59,
                    "Description": "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius"
                },
                {
                    "Id": 27,
                    "Description": "(UTC+02:00) Jerusalem"
                },
                {
                    "Id": 7,
                    "Description": "(UTC+02:00) Minsk (old)"
                },
                {
                    "Id": 104,
                    "Description": "(UTC+02:00) E. Europe"
                },
                {
                    "Id": 100,
                    "Description": "(UTC+02:00) Kaliningrad"
                },
                {
                    "Id": 26,
                    "Description": "(UTC+03:00) Baghdad"
                },
                {
                    "Id": 101,
                    "Description": "(UTC+03:00) Istanbul"
                },
                {
                    "Id": 74,
                    "Description": "(UTC+03:00) Kuwait, Riyadh"
                },
                {
                    "Id": 109,
                    "Description": "(UTC+03:00) Minsk"
                },
                {
                    "Id": 51,
                    "Description": "(UTC+03:00) Moscow, St. Petersburg, Volgograd"
                },
                {
                    "Id": 56,
                    "Description": "(UTC+03:00) Nairobi"
                },
                {
                    "Id": 25,
                    "Description": "(UTC+03:30) Tehran"
                },
                {
                    "Id": 24,
                    "Description": "(UTC+04:00) Abu Dhabi, Muscat"
                },
                {
                    "Id": 110,
                    "Description": "(UTC+04:00) Astrakhan, Ulyanovsk"
                },
                {
                    "Id": 54,
                    "Description": "(UTC+04:00) Baku"
                },
                {
                    "Id": 106,
                    "Description": "(UTC+04:00) Izhevsk, Samara"
                },
                {
                    "Id": 89,
                    "Description": "(UTC+04:00) Port Louis"
                },
                {
                    "Id": 82,
                    "Description": "(UTC+04:00) Tbilisi"
                },
                {
                    "Id": 84,
                    "Description": "(UTC+04:00) Yerevan"
                },
                {
                    "Id": 48,
                    "Description": "(UTC+04:30) Kabul"
                },
                {
                    "Id": 58,
                    "Description": "(UTC+05:00) Ekaterinburg"
                },
                {
                    "Id": 87,
                    "Description": "(UTC+05:00) Islamabad, Karachi"
                },
                {
                    "Id": 47,
                    "Description": "(UTC+05:00) Tashkent"
                },
                {
                    "Id": 23,
                    "Description": "(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi"
                },
                {
                    "Id": 66,
                    "Description": "(UTC+05:30) Sri Jayawardenepura"
                },
                {
                    "Id": 62,
                    "Description": "(UTC+05:45) Kathmandu"
                },
                {
                    "Id": 71,
                    "Description": "(UTC+06:00) Astana"
                },
                {
                    "Id": 102,
                    "Description": "(UTC+06:00) Dhaka"
                },
                {
                    "Id": 115,
                    "Description": "(UTC+06:00) Omsk"
                },
                {
                    "Id": 61,
                    "Description": "(UTC+06:30) Yangon (Rangoon)"
                },
                {
                    "Id": 22,
                    "Description": "(UTC+07:00) Bangkok, Hanoi, Jakarta"
                },
                {
                    "Id": 111,
                    "Description": "(UTC+07:00) Barnaul, Gorno-Altaysk"
                },
                {
                    "Id": 64,
                    "Description": "(UTC+07:00) Krasnoyarsk"
                },
                {
                    "Id": 46,
                    "Description": "(UTC+07:00) Novosibirsk"
                },
                {
                    "Id": 112,
                    "Description": "(UTC+07:00) Tomsk"
                },
                {
                    "Id": 45,
                    "Description": "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi"
                },
                {
                    "Id": 63,
                    "Description": "(UTC+08:00) Irkutsk"
                },
                {
                    "Id": 21,
                    "Description": "(UTC+08:00) Kuala Lumpur, Singapore"
                },
                {
                    "Id": 73,
                    "Description": "(UTC+08:00) Perth"
                },
                {
                    "Id": 75,
                    "Description": "(UTC+08:00) Taipei"
                },
                {
                    "Id": 94,
                    "Description": "(UTC+08:00) Ulaanbaatar"
                },
                {
                    "Id": 20,
                    "Description": "(UTC+09:00) Osaka, Sapporo, Tokyo"
                },
                {
                    "Id": 72,
                    "Description": "(UTC+09:00) Seoul"
                },
                {
                    "Id": 70,
                    "Description": "(UTC+09:00) Yakutsk"
                },
                {
                    "Id": 19,
                    "Description": "(UTC+09:30) Adelaide"
                },
                {
                    "Id": 44,
                    "Description": "(UTC+09:30) Darwin"
                },
                {
                    "Id": 18,
                    "Description": "(UTC+10:00) Brisbane"
                },
                {
                    "Id": 76,
                    "Description": "(UTC+10:00) Canberra, Melbourne, Sydney"
                },
                {
                    "Id": 43,
                    "Description": "(UTC+10:00) Guam, Port Moresby"
                },
                {
                    "Id": 42,
                    "Description": "(UTC+10:00) Hobart"
                },
                {
                    "Id": 99,
                    "Description": "(UTC+10:00) Magadan"
                },
                {
                    "Id": 68,
                    "Description": "(UTC+10:00) Vladivostok"
                },
                {
                    "Id": 107,
                    "Description": "(UTC+11:00) Chokurdakh"
                },
                {
                    "Id": 114,
                    "Description": "(UTC+11:00) Sakhalin"
                },
                {
                    "Id": 41,
                    "Description": "(UTC+11:00) Solomon Is., New Caledonia"
                },
                {
                    "Id": 108,
                    "Description": "(UTC+12:00) Anadyr, Petropavlovsk-Kamchatsky"
                },
                {
                    "Id": 17,
                    "Description": "(UTC+12:00) Auckland, Wellington"
                },
                {
                    "Id": 97,
                    "Description": "(UTC+12:00) Coordinated Universal Time+12"
                },
                {
                    "Id": 40,
                    "Description": "(UTC+12:00) Fiji"
                },
                {
                    "Id": 92,
                    "Description": "(UTC+12:00) Petropavlovsk-Kamchatsky - Old"
                },
                {
                    "Id": 67,
                    "Description": "(UTC+13:00) Nuku'alofa"
                },
                {
                    "Id": 16,
                    "Description": "(UTC+13:00) Samoa"
                }
            ];
            this.scroll.yOffset = 150;
            auth.acquireToken().then(function () {
                _this.getWorkspace();
                return dataService.GetUsers().then(function (x) {
                    _this.loading = false;
                    _this.ownerPlaceHolder = "Search for users";
                    _this.users = x;
                });
            }).catch(function (e) { });
            this.dataWorkspace = new Workspace_1.default();
            this.workspaceNamePlaceholder = config.workspaceNamePlaceholder;
        }
        Object.defineProperty(WorkspacesComponent.prototype, "userCountries", {
            get: function () { return this.auth.userCountries; },
            enumerable: true,
            configurable: true
        });
        WorkspacesComponent.prototype.addOwners = function (users) {
            var _this = this;
            users.forEach(function (u) {
                if (!_this.dataWorkspace.Owners.some(function (o) { return o.Id == u.Id; })) {
                    _this.dataWorkspace.Owners.push(u);
                }
            });
        };
        WorkspacesComponent.prototype.removeOwners = function (users) {
            this.dataWorkspace.Owners = this.dataWorkspace.Owners.filter(function (o) { return !users.some(function (u) { return u.Id === o.Id; }); });
        };
        WorkspacesComponent.prototype.showCreateForm = function () {
            this.scroll("edit");
            this.isCreateMode = true;
            this.isOwnersMode = false;
            this.isEditMode = false;
            this.selectedWorkspace = undefined;
        };
        WorkspacesComponent.prototype.showEditForm = function (workspace) {
            console.log(workspace);
            this.scroll("edit");
            this.isCreateMode = false;
            this.isOwnersMode = false;
            this.isEditMode = true;
            this.selectedWorkspace = __assign({}, workspace);
        };
        WorkspacesComponent.prototype.showOwnersForm = function (workspace) {
            this.scroll("edit");
            this.isCreateMode = false;
            this.isEditMode = false;
            this.isOwnersMode = true;
            this.selectedWorkspace = workspace;
        };
        WorkspacesComponent.prototype.hideEditForm = function () {
            this.dataWorkspace = new Workspace_1.default();
            this.isEditMode = false;
        };
        WorkspacesComponent.prototype.hideCreateForm = function () {
            this.dataWorkspace = new Workspace_1.default();
            this.isCreateMode = false;
        };
        WorkspacesComponent.prototype.sendWorkspace = function () {
            var _this = this;
            this.saving = true;
            this.dataService.PutWorkspace(this.dataWorkspace)
                .then(function (newWorkspace) {
                _this.hideCreateForm();
                _this.getWorkspace();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Workspace saved";
                _this.notify(c);
                _this.logService.addMessage("Workspace " + newWorkspace.Name + " created", "Trace");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            }).finally(function () { _this.saving = false; });
        };
        WorkspacesComponent.prototype.saveWorkspace = function () {
            var _this = this;
            this.saving = true;
            var workspaceNewId = this.selectedWorkspace ? this.selectedWorkspace.Id : "";
            var workspaceNewUrl = this.selectedWorkspace ? this.selectedWorkspace.Url : "";
            var workspaceNewName = this.selectedWorkspace ? this.selectedWorkspace.Name : "";
            var datasToUpdate = {
                Url: workspaceNewUrl,
                Name: workspaceNewName
            };
            console.log("datasToUpdate", datasToUpdate);
            this.dataService.UpdateWorkspace(workspaceNewId, datasToUpdate)
                .then(function (x) {
                _this.hideEditForm();
                _this.getWorkspace();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Workspace saved";
                _this.notify(c);
                _this.logService.addMessage("Workspace " + name + " updated", "Trace");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            }).finally(function () { _this.saving = false; });
        };
        WorkspacesComponent.prototype.getWorkspace = function () {
            var _this = this;
            this.loading = true;
            this.dataService.GetWorkspaces().then(function (x) {
                console.log(x);
                _this.workspaces = x;
                // this.workspaces.forEach(w => w.Url = this.config.sharePointBaseUrl + '/' + encodeURIComponent(w.Name));
                _this.loading = false;
            });
        };
        WorkspacesComponent.prototype.isMatchFilter = function (filter) {
            return function (value) { return value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1; };
        };
        WorkspacesComponent.prototype.matchUser = function (filter) {
            var _this = this;
            return function (u) {
                var isMatch = _this.isMatchFilter(filter);
                return (filter == null
                    || filter.length === 0
                    || isMatch(u.DisplayName)
                    || isMatch(u.City)
                    || isMatch(u.Department)
                    || isMatch(u.Country)
                    || isMatch(u.JobTitle)
                    || isMatch(u.Mail)
                    || isMatch(u.UserPrincipalName));
            };
        };
        WorkspacesComponent.prototype.tryDelete = function (workspace) {
            var _this = this;
            var modal = this.modal.open({
                size: 'lg',
                controller: DeleteWorkspaceController,
                controllerAs: "ctrl",
                templateUrl: 'app/views/deleteWorkspace.html',
                resolve: {
                    "workspace": workspace
                }
            });
            modal.closed.catch(function () { });
            modal.result.then(function () { return _this.dataService.DeleteWorkspace(workspace); }).then(function () {
                _this.getWorkspace();
                var notification = angular.copy(_this.notifyOkConfig);
                notification.message = "Workspace \"" + workspace.Name + "\" has been successfully deleted";
                _this.notify(notification);
                _this.logService.addMessage("Workspace \"" + workspace.Name + "\" has been successfully deleted", "Info");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            });
        };
        WorkspacesComponent.$inject = ["$exceptionHandler", "$anchorScroll", "$uibModal", "authenticationService", "dataService", "feedbackService", "notify", "notifyOkConfig", "config"];
        WorkspacesComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'workspaces', {
                controllerAs: 'ctrl',
                templateUrl: 'app/views/workspaces.html'
            })
        ], WorkspacesComponent);
        return WorkspacesComponent;
    }());
    exports.WorkspacesComponent = WorkspacesComponent;
    var DeleteWorkspaceController = /** @class */ (function () {
        function DeleteWorkspaceController($uibModalInstance, workspace) {
            this.$uibModalInstance = $uibModalInstance;
            this.workspace = workspace;
        }
        DeleteWorkspaceController.prototype.cancel = function () {
            this.$uibModalInstance.dismiss();
        };
        DeleteWorkspaceController.prototype.ok = function () {
            this.$uibModalInstance.close();
        };
        DeleteWorkspaceController.$inject = ["$uibModalInstance", "workspace"];
        return DeleteWorkspaceController;
    }());
});
//# sourceMappingURL=WorkspacesComponent.js.map