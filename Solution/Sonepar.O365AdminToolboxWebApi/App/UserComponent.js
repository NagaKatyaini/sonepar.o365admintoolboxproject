var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "./ComponentDecorator"], function (require, exports, app_1, ComponentDecorator_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var UserComponent = /** @class */ (function () {
        function UserComponent($filter, auth, dataService, $exceptionHandler) {
            var _this = this;
            this.$filter = $filter;
            this.auth = auth;
            this.dataService = dataService;
            this.$exceptionHandler = $exceptionHandler;
            this.users = [];
            this.LicensesRef = [];
            this.licencesFriendlyNames = [];
            this.licencesTranslationsChanged = [];
            this.languageList = [];
            this.loading = false;
            this.inited = false;
            this.isEditLicenceNamesMode = false;
            this.savingLicenceNames = false;
            this.isCurrentUserLicenceManager = false;
            this.licensed = null;
            dataService.GetLicenses().then(function (licences) {
                _this.LicensesRef = licences;
                _this.getLicencesFriendlyNames(licences);
                _this.load(false);
            });
            dataService.IsCurrentUserLicenceManager().then(function (response) {
                _this.isCurrentUserLicenceManager = response;
                console.log('isCurrentUserLicenceManager', response);
            });
        }
        UserComponent.prototype.isMatch = function (value) {
            return value != null && value.toLowerCase().indexOf(this.filter.toLowerCase()) > -1;
        };
        UserComponent.prototype.matchUser = function (u) {
            return (this.filter == null
                || this.filter.length === 0
                || this.isMatch(u.DisplayName)
                || this.isMatch(u.City)
                || this.isMatch(u.Department)
                || this.isMatch(u.Country)
                || this.isMatch(u.JobTitle)
                || this.isMatch(u.Mail)
                || this.isMatch(u.UserPrincipalName)) && (this.licensed === null
                || this.licensed && u.HasLicenseAssigned
                || !this.licensed && !u.HasLicenseAssigned);
        };
        UserComponent.prototype.formatUser = function (user) {
            return user.DisplayName + ' (' + user.GroupIds.replace(/;/g, ",") + ')'; //+ (user.AssignedLicenses.length > 0 ? '' : ' * ');
        };
        UserComponent.prototype.toggleLicensed = function () {
            if (this.licensed == null) {
                this.licensed = true;
            }
            else if (this.licensed == true) {
                this.licensed = false;
            }
            else {
                this.licensed = null;
            }
        };
        UserComponent.prototype.CancelEditLicences = function () {
            this.isEditLicenceNamesMode = false;
        };
        UserComponent.prototype.translationChanged = function (line) {
            console.log(line);
            var foundLine = this.licencesTranslationsChanged.find(function (x) { return x.LicenceGuid === line.Name; });
            if (!foundLine) {
                this.licencesTranslationsChanged.push({
                    LicenceGuid: line.Name,
                    FriendlyName: line.FriendlyName
                });
            }
            else {
                var foundIndex = this.licencesTranslationsChanged.findIndex(function (x) { return x.LicenceGuid == line.Name; });
                this.licencesTranslationsChanged[foundIndex].FriendlyName = line.FriendlyName;
            }
            console.log(this.licencesTranslationsChanged);
        };
        UserComponent.prototype.saveUpdatedLicences = function () {
            var _this = this;
            this.savingLicenceNames = true;
            this.dataService.UpdateLicencesNames(this.licencesTranslationsChanged).then(function (response) {
                console.log(response);
                _this.savingLicenceNames = false;
                window.location.reload();
            });
        };
        UserComponent.prototype.getLicencesFriendlyNames = function (licences) {
            var friendlyNameLicenceList = [];
            this.dataService.GetLicensesNames().then(function (allLicencesNames) {
                console.log(licences);
                console.log(allLicencesNames);
                licences.forEach(function (licence) {
                    var friendlyNameLicence = allLicencesNames.filter(function (licenceToFind) { return licenceToFind.LicenceGuid === licence.Name; });
                    var licenceFriendlyName;
                    if (friendlyNameLicence.length !== 0) {
                        licenceFriendlyName = friendlyNameLicence[0].FriendlyName;
                    }
                    else {
                        licenceFriendlyName = licence.Name;
                    }
                    var friendlyNameList = [];
                    licence.Services.forEach(function (serviceObject) {
                        var friendlyNameServiceToFind = allLicencesNames.filter(function (serviceToFind) { return serviceToFind.LicenceGuid === serviceObject.Service.Name; });
                        var serviceFriendlyName;
                        if (friendlyNameServiceToFind.length !== 0) {
                            serviceFriendlyName = friendlyNameServiceToFind[0].FriendlyName;
                        }
                        else {
                            serviceFriendlyName = serviceObject.Service.Name;
                        }
                        friendlyNameList.push({
                            Name: serviceObject.Service.Name,
                            FriendlyName: serviceFriendlyName
                        });
                    });
                    friendlyNameLicenceList.push({
                        Name: licence.Name,
                        FriendlyName: licenceFriendlyName,
                        services: friendlyNameList
                    });
                });
            });
            console.log(friendlyNameLicenceList);
            this.licencesFriendlyNames = friendlyNameLicenceList;
        };
        UserComponent.prototype.load = function (refresh) {
            var _this = this;
            if (refresh === void 0) { refresh = true; }
            return this.auth.acquireToken().then(function () {
                _this.loading = true;
                _this.users = [];
            }).then(function () {
                return _this.dataService.GetUsers(refresh).then(function (x) {
                    console.log(x);
                    _this.users = x;
                    _this.loading = false;
                    _this.inited = true;
                });
            }
            // this.dataService.GetUsers().then(users => console.log(users)));
            // this.dataService.GetUserGroups(false).then(groups => {
            //    console.log(groups);
            //    groups.forEach(async group => {
            //        this.loading = true;
            //        // this.getGroupUsers(group, "");
            //        this.dataService.GetUsers().then(users => console.log(users));
            //        let usersToConcat:User[] = []
            //        await this.dataService.GetGroupAllMembers(group.id).then(users => {
            //            console.log(users);
            //            users.forEach(user => {
            //                if (this.users.some(presentUser => presentUser.UserPrincipalName === user.UserPrincipalName)) {
            //                    console.log("existing !");
            //                    user.DisplayName = user.DisplayName.replace(')', ` ${group.displayName})`);
            //                    let objIndex = this.users.findIndex((u => u.UserPrincipalName === user.UserPrincipalName));
            //                    this.users[objIndex].DisplayName = this.users[objIndex].DisplayName.replace(')', `, ${group.displayName.substring(4, 6)})`);                                
            //                }
            //                else {
            //                    usersToConcat.push({ ...user, DisplayName: `${user.DisplayName} (${group.displayName.substring(4, 6)})` });
            //                }
            //            })
            //            this.users = this.users.concat(usersToConcat);
            //            this.loading = false;
            //        });
            //    })
            //})
            ).catch(function (e) { return _this.$exceptionHandler(e); });
        };
        UserComponent.prototype.getGroupUsers = function (group, skipToken) {
            var _this = this;
            console.log(group);
            this.loading = true;
            var payload = {
                groupId: group.id,
                groupDisplayName: group.displayName,
                skipToken: skipToken
            };
            this.dataService.GetGroupMembersByPage(payload).then(function (pagedUsersResponse) {
                console.log(pagedUsersResponse);
                var result = [];
                var map = new Map();
                for (var _i = 0, _a = _this.users; _i < _a.length; _i++) {
                    var item = _a[_i];
                    if (!map.has(item.UserPrincipalName)) {
                        map.set(item.UserPrincipalName, true);
                        result.push(item);
                    }
                }
                _this.users = result;
                _this.users.sort(function (a, b) {
                    if (a.DisplayName < b.DisplayName) {
                        return -1;
                    }
                    if (a.DisplayName > b.DisplayName) {
                        return 1;
                    }
                    return 0;
                });
                if (pagedUsersResponse.skipToken !== "") {
                    _this.getGroupUsers(group, pagedUsersResponse.skipToken);
                }
                else {
                    // this.loading = false;
                }
            });
        };
        UserComponent.prototype.editLicences = function () {
            this.isEditLicenceNamesMode = true;
            this.selectedUsers = [];
            console.log(this.selectedUsers);
        };
        UserComponent.prototype.onUserSelection = function () {
            this.isEditLicenceNamesMode = false;
        };
        UserComponent.prototype.refresh = function () {
            this.load(true);
        };
        UserComponent.$inject = ["$filter", "authenticationService", "dataService", "$exceptionHandler"];
        UserComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'users', {
                controllerAs: 'ctrl',
                templateUrl: 'app/views/users.html'
            })
        ], UserComponent);
        return UserComponent;
    }());
    exports.UserComponent = UserComponent;
});
//# sourceMappingURL=UserComponent.js.map