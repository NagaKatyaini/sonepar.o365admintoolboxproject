﻿import { app } from './app';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import Country from './Model/Country';
import Component from './ComponentDecorator';
import Group from './Model/Group';
import User from './Model/User';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IFeedbackService } from './Services/FeedbackService';

import * as angular from 'angular';

@Component(app, 'groupowners', {
    controllerAs: 'ctrl',
    bindings: { "group": "<", onSaved: "&" },
    templateUrl: 'app/views/groupowners.html'
}) export class GroupOwnersComponent {
    public users: User[] | undefined;
    private _group: Group;
    public onSaved: () => void;
    public get group(): Group {
        return this._group;
    }
    public set group(g: Group) {
        this._group = g;
        if (g) {
            this.load();
        }
    }
    public loading: boolean = false;
    public saving: boolean = false;
    public filter: string;

    private load() {
        this.users = undefined;
        this.dataService.GetGroupOwners(this._group.Id).then((u) => this.users = u);
    }
    private saved() {
        this.onSaved && this.onSaved();
    }
    private feedback(a: string, member: User) {
        this.logService.addMessage(`Group ${this.group.Name}, owner ${member.DisplayName} ${a}`, "Trace");
    }
    public save() {
        this.saving = true;
        this.dataService.UpdateGroupOwners(this._group.Id, this.users || [], this.feedback.bind(this))
            .then(() => {
                this.saved();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Owners saved";
                this.notify(c);
                this.logService.addMessage("Operation finished successfully", "Info");
            })
            .catch((e) => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.logService.addMessage("Operation cancelled", "Error");
                this.$exceptionHandler(e);
            })
            .finally(() => { this.saving = false; });
    }
    constructor(
        private $q: ng.IQService,
        private $exceptionHandler: ng.IExceptionHandlerService,
        private auth: IAuthenticationService,
        private logService: IFeedbackService,
        private dataService: IDataService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions) {
    }
    public static $inject = ["$q", "$exceptionHandler", "authenticationService", "feedbackService", "dataService", "notify", "notifyOkConfig"];
}
