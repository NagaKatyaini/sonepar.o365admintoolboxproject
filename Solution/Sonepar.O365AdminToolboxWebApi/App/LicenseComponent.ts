import { app } from './app';
import * as angular from 'angular';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import { IFeedbackService } from './Services/FeedbackService';
import Country from './Model/Country';
import Component from './ComponentDecorator';
import User from './Model/User';
import License from './Model/License';
import { LicenseAssignment } from './Model/License';

@Component(app, 'license', {
    controllerAs: 'ctrl',
    bindings: { "users": "<", 'onSaved': "&", 'onChange':"&" },
    templateUrl: 'app/views/license.html'
}) export class LicenseComponent {
    public users: User[] | User;
    private get usersCol() {
        var col: User[] = [];
        if (!angular.isArray(this.users)) {
            col = [<User>this.users];
        } else {
            col = <User[]>this.users;
        }
        return col;
    }
    // public get user(): User | undefined {
    public get user(): User {
        return this.users && (
            angular.isArray(this.users) && this.users && this.users.length === 1 && this.users[0]
            || <User>this.users
        ) || undefined;
    }
    public counters: { [key: string]: number };
    public IsMFAEnable: boolean = false;
    public IsLoadingMFAStatut: boolean = true;
    public countries: Country[];
    public LicensesRef: License[] = [];
    public Licenses: LicenseAssignment[] ;
    public loading: boolean = false;
    public saving: boolean = false;
    public filter: string;
    public onSaved: () => void;
    public userForm: ng.IFormController;
    public UsageLocation: string | undefined;
    public filterFilter: ng.IFilterFilter;
    private loadedProm: ng.IPromise<any>;

    public static $inject = [
        "$q",
        "$exceptionHandler",
        "authenticationService",
        "dataService",
        "feedbackService",
        "notify", "notifyOkConfig"];
    constructor(
        private $q: ng.IQService,
        private $exceptionHandler: ng.IExceptionHandlerService,
        private auth: IAuthenticationService,
        private dataService: IDataService,
        private logservice: IFeedbackService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions
    ) {
        auth.acquireToken().then(() => {
            // this.loading = true;
        });
        this.loadedProm = this.$q.all([
            dataService.GetLicenses().then(x => {
                console.log(x);
                this.LicensesRef = x;
            }),
            
            dataService.GetCountries().then(x => {
                this.countries = x;
            })]).then(() => {
                this.loading = false;
            });
        this.$onChanges = this.onChanges.bind(this);
        this.$changedUserStatut = this.changedUserStatut.bind(this);
        
    }
    private feedback(a: string, u: User, licName: string) {
        this.logservice.addMessage(`User ${u.DisplayName}, license ${licName} ${a}`, "Trace");
        this.counters[a] = (this.counters[a] || 0) + 1;
    }

    public save(userChanged: boolean) {
        this.saving = true;
        this.counters = {};
        this.Licenses.forEach(l => l.License.UsageLocation = this.UsageLocation);
        return this.dataService.SaveLicenses(this.usersCol, this.Licenses, this.feedback.bind(this)).then(() => {
            this.dataService.GetLicenses().then(x => {
                this.LicensesRef = x;
            });
        }).then(async () => {
            var c = angular.copy(this.notifyOkConfig);
            c.message = "User licenses saved :";
            c.message += this.makeMessage();
            this.notify(c);
            this.logservice.addMessage("Operation finished successfully", "Info");
            if (this.onSaved) { this.onSaved() };

        }).catch(e => {
            var errorMessage: any = "List is not up to date any longer, please hit refresh";

            this.logservice.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
            this.logservice.addMessage("Operation cancelled", "Error");
            this.$exceptionHandler(errorMessage);
            this.$exceptionHandler(e, this.makeMessage());
        }).finally(() => {
            this.saving = false
        });
    }

    private makeMessage() {
        var m = "";
        for (var a in this.counters) {
            m = (m && (m + ",")) + `${this.counters[a]} ${a}`;
        }
        return m;
    }

    public $changedUserStatut: (x: ng.IOnChangesObject) => void;

    public changedUserStatut() {
        if (this.userForm) { this.userForm.$setPristine(); }
        let accountStateToSet = this.user.AccountEnabled ? true : false;
        console.log("accountStatut to Set", accountStateToSet);
        return this.auth.acquireToken().then(() => {
            // this.loading = true;
        }).then(() =>
            this.dataService.UpdateUserStatut(this.user, accountStateToSet).then(x => {
                this.user.AccountEnabled = accountStateToSet;
                let newStatut = accountStateToSet ? "enabled" : "disabled"
                var c = angular.copy(this.notifyOkConfig);
                c.message = `Account ${this.user.UserPrincipalName} ${newStatut}`;
                c.message += this.makeMessage();
                this.notify(c);
                this.logservice.addMessage(`Account ${this.user.UserPrincipalName} ${newStatut} successfully`, "Info");
            })
        ).catch(e => this.$exceptionHandler(e))
            .finally(() => { this.loading = false });
    }

    public resetMFA() {
        if (this.userForm) { this.userForm.$setPristine(); }
        alert("hi");
            this.dataService.ResetMFA(this.user).then(response => {
                var c = angular.copy(this.notifyOkConfig);
                c.message = `Account ${this.user.UserPrincipalName} MFA Reset successfully`;
                c.message += this.makeMessage();
                this.notify(c);
                this.logservice.addMessage(`Account ${this.user.UserPrincipalName} MFA reset successfully`, "Info");
            })
        
    }

    public changedUserMFAStatut() {
        if (this.userForm) { this.userForm.$setPristine(); }
        this.IsLoadingMFAStatut = true;
        if (this.IsMFAEnable) {
            this.dataService.UpdateUserMFAStatut(this.user, "enabled").then(response => {
                this.IsLoadingMFAStatut = false;
                let newStatut = "enabled";
                var c = angular.copy(this.notifyOkConfig);
                c.message = `Account ${this.user.UserPrincipalName} MFA ${newStatut}`;
                c.message += this.makeMessage();
                this.notify(c);
                this.logservice.addMessage(`Account ${this.user.UserPrincipalName} MFA ${newStatut} successfully`, "Info");
            })
        }
        else {
            this.dataService.UpdateUserMFAStatut(this.user, "disabled").then(response => {
                this.IsLoadingMFAStatut = false;
                let newStatut = "disable";
                var c = angular.copy(this.notifyOkConfig);
                c.message = `Account ${this.user.UserPrincipalName} MFA ${newStatut}`;
                c.message += this.makeMessage();
                this.notify(c);
                this.logservice.addMessage(`Account ${this.user.UserPrincipalName} MFA ${newStatut} successfully`, "Info");
            })
        }                
    }

    public $onChanges: (x: ng.IOnChangesObject) => void;

    public onChanges(x: ng.IOnChangesObject) {
        
        if (this.LicensesRef) {
            console.log(this.LicensesRef);
            this.onChangesSync(x);
        } else {
            this.loadedProm.then(() => {
                console.log(x);
                this.onChangesSync(x);
            });
        }
    }
    public async onChangesSync(x: ng.IOnChangesObject) {
        console.log(x);
        var val: User[] | User = x['users'].currentValue;
        var col: any[] = [];
        if (!val) {
            this.Licenses = [];
        }
        else if (!angular.isArray(val)) {
            col = [<User>val];
        } else {
            col = <User[]>val;
        }
        console.log(col);
        console.log(col.length);
        if (col.length == 1) {
            this.loading = true;
            let selectedUser = await this.dataService.GetUser(col[0].Id);
            this.loading = false;
            this.IsLoadingMFAStatut = true;
            this.dataService.GetUserMFAStatut(selectedUser).then(response => {
                this.IsLoadingMFAStatut = false;
                if (response.userStatut == "Enabled") {
                    this.IsMFAEnable = true;
                }
                else {
                    this.IsMFAEnable = false;
                }
            });
            console.log(selectedUser);
            this.users = [selectedUser];
            
            this.Licenses = this.LicensesRef.map(x => new LicenseAssignment(x, selectedUser.AssignedLicenses.some(l => this.eq(l.Id, x.Id))));
            this.Licenses.forEach(l => {
                var al = selectedUser.AssignedLicenses.filter(al => this.eq(al.Id, l.License.Id))[0];
                l.License.Services.forEach(
                    s => {
                        s.IsActive = (!al) || al.Services.some(p => this.eq(p.Service.Id, s.Service.Id) && (p.IsActive || false));
                    });
            });
            this.UsageLocation = selectedUser.UsageLocation;
        } else if (col.length > 1) {
            this.loading = false;
            this.Licenses = this.LicensesRef.map(x => new LicenseAssignment(x));
            this.Licenses.forEach(l => l.License.Services.forEach(s => s.IsActive = undefined));
            this.UsageLocation = col.reduce((p: string | null | undefined, n: User) => p === undefined ? n.UsageLocation : p === n.UsageLocation ? p : null, undefined) || undefined;
        }
        if (this.userForm) { this.userForm.$setPristine(); }        
    }

    public requireCountry(): boolean {
        return this.usersCol.some(u => !u.UsageLocation) && (this.usersCol.length == 1 || this.Licenses.some(x => x.IsActive === true));
    }
    private eq(a?: string, b?: string) {
        return a && b && a.toLowerCase() === b.toLowerCase() || false;
    }
}
