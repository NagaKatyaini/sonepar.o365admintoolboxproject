﻿import { app } from './app';
import * as angular from 'angular';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IFeedbackService } from './Services/FeedbackService';
import { IConfig } from 'interfaces/Config';

import Component from './ComponentDecorator';
import User from './Model/User';
import Group from './Model/Group';

@Component(app, 'groups', {
    controllerAs: 'ctrl',
    templateUrl: 'app/views/groups.html'
}) export class GroupsComponent {
    public get userCountries(): string[] { return this.auth.userCountries; }
    public groups: Group[];
    public users: User[];
    public groupOwners: User[];
    public dataGroup: Group;
    public ownerPlaceHolder: string = 'Loading users...';
    public message: string;
    public saving: boolean = false;
    public loading: boolean = false;
    public isCreateMode: boolean = false;
    public isOwnersMode: boolean = false;
    public isMembersMode: boolean = false;
    public isEditGroupMode: boolean = false;
    public selectedGroup: Group | undefined;
    public ownerGroupId: string;
    public groupNamePlaceholder: string;

    public addOwners(users: User[]) {
        users.forEach(u => {
            if (!this.dataGroup.Owners.some(o => o.Id == u.Id)) {
                this.dataGroup.Owners.push(u);
            }
        })
    }
    public removeOwners(users: User[]) {
        this.dataGroup.Owners = this.dataGroup.Owners.filter(o => !users.some(u => u.Id === o.Id));
    }

    public showCreateForm() {
        this.scroll("edit")
        this.isCreateMode = true;
        this.isMembersMode = false;
        this.isOwnersMode = false;
        this.selectedGroup = undefined;
        this.isEditGroupMode = false;
    }

    public showEditForm(group: Group) {
        console.log(group);
        this.selectedGroup = { ...group };
        this.scroll("edit")
        this.isEditGroupMode = true;
        this.isCreateMode = false;
        this.isMembersMode = false;
        this.isOwnersMode = false;
    }

    

    public showMembersForm(group: Group) {
        this.scroll("edit")
        this.isMembersMode = true;
        this.isCreateMode = false;
        this.isOwnersMode = false;
        this.selectedGroup = group;
        this.isEditGroupMode = false;
    }
    public showOwnersForm(group: Group) {
        this.scroll("edit")
        this.isMembersMode = false;
        this.isCreateMode = false;
        this.isOwnersMode = true;
        this.selectedGroup = group;
        this.isEditGroupMode = false;
    }

    public hideEditForm() {
        console.log("hideEditForm");
        this.dataGroup = new Group();
        this.isEditGroupMode = false;
    }

    public hideCreateForm() {
        this.dataGroup = new Group();
        this.isCreateMode = false;
    }

    public sendGroup() {
        this.saving = true;
        var name = this.dataGroup.Name;
        this.dataService.PutGroup(this.dataGroup)
            .then(x => {
                this.hideCreateForm();
                this.getGroup();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Group saved";
                this.notify(c);
                this.logService.addMessage(`Group ${name} created`, "Trace");
                this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e);
            }).finally(() => { this.saving = false })
    }

    public saveGroup() {
        this.saving = true;
        let groupId = this.selectedGroup ? this.selectedGroup.Id : "";
        let groupName = this.selectedGroup ? this.selectedGroup.Name : "";
        let groupDescription = this.selectedGroup ? this.selectedGroup.Description : "";
        let datasToUpdate = {
            Id: groupId,
            Name: groupName,
            Description: groupDescription
        }
        console.log("groupName", groupName);
        console.log("groupDescription", groupDescription);

        this.dataService.UpdateGroup(datasToUpdate)
            .then(x => {                
                this.getGroup();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Group saved";
                this.notify(c);
                this.logService.addMessage(`Group ${name} updated`, "Trace");
                this.logService.addMessage("Operation finished successfully", "Info");
                this.hideEditForm();
            }).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e);
            }).finally(() => { this.saving = false })
    }

    public getGroup() {
        this.loading = true;
        this.dataService.GetGroups().then(x => {
            this.groups = x;
            this.loading = false;
        });
    }

    private isMatchFilter(filter: string) {

        return (value: string) => value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1;
    }
    public matchUser(filter: string) {
        return (u: User) => {
            var isMatch = this.isMatchFilter(filter);
            return (filter == null
                || filter.length === 0
                || isMatch(u.DisplayName)
                || isMatch(u.City)
                || isMatch(u.Department)
                || isMatch(u.Country)
                || isMatch(u.JobTitle)
                || isMatch(u.Mail)
                || isMatch(u.UserPrincipalName));
        };
    }

    public tryDelete(group: Group) {
        var modal = this.modal.open({
            size: 'lg',
            controller: DeleteGroupController,
            controllerAs: "ctrl",
            templateUrl: 'app/views/deleteGroup.html',
            resolve: {
                "group": group
            }
        });
        modal.closed.catch(() => { });
        modal.result.then(() => this.dataService.DeleteGroup(group)
        ).then(
            () => {
                this.getGroup();
                var notification = angular.copy(this.notifyOkConfig);
                notification.message = `Group "${group.Name}" has been successfully deleted`
                this.notify(notification);
                this.logService.addMessage(`Group "${group.Name}" has been successfully deleted`, "Info");
                this.logService.addMessage("Operation finished successfully", "Info");
            }
        ).catch(e => {
           this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
           this.$exceptionHandler(e)
        });
    }

    constructor(
        private $exceptionHandler: ng.IExceptionHandlerService,
        private scroll: ng.IAnchorScrollService,
        private modal : ng.ui.bootstrap.IModalService,
        private auth: IAuthenticationService,
        private dataService: IDataService,
        private logService: IFeedbackService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions,
        private config: IConfig
    ) {
        this.scroll.yOffset = 150;
        auth.acquireToken().then(() => {
            this.getGroup();
            return dataService.GetUsers().then(x => {
                this.loading = false;
                this.ownerPlaceHolder = "Search for users"
                this.users = x;
            });
        }).catch(e => { });
        this.dataGroup = new Group();
        this.groupNamePlaceholder = config.groupNamePlaceholder;
    }

    public static $inject = ["$exceptionHandler", "$anchorScroll", "$uibModal", "authenticationService", "dataService", "feedbackService", "notify", "notifyOkConfig", "config"];
}

class DeleteGroupController{
    constructor(
        private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, private group:Group
    ) {
        
    }
    public cancel() {
        this.$uibModalInstance.dismiss();
    }
    public ok() {
        this.$uibModalInstance.close();
    }

    public static $inject = ["$uibModalInstance","group"];

}