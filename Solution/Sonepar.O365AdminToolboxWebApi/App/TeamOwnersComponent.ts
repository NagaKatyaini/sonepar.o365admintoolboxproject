﻿import { app } from './app';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import Country from './Model/Country';
import Component from './ComponentDecorator';
import OfficeObject from './Model/OfficeObject';
import User from './Model/User';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IFeedbackService } from './Services/FeedbackService';

import * as angular from 'angular';

@Component(app, 'teamowners', {
    controllerAs: 'ctrl',
    bindings: { "team": "<", onSaved: "&" },
    templateUrl: 'app/views/teamowners.html'
}) export class TeamOwnersComponent {
    public users: User[] | undefined;
    private _team: OfficeObject;
    public onSaved: () => void;
    public get team(): OfficeObject {
        return this._team;
    }
    public set team(g: OfficeObject) {
        this._team = g;
        if (g) {
            this.load();
        }
    }
    public loading: boolean = false;
    public saving: boolean = false;
    public filter: string;

    private load() {
        this.users = undefined;
        this.dataService.GetTeamOwners(this._team.Id).then((u) => this.users = u);
    }
    private saved() {
        this.onSaved && this.onSaved();
    }
    private feedback(a: string, member: User) {
        this.logService.addMessage(`Team ${this.team.Name}, owner ${member.DisplayName} ${a}`, "Trace");
    }
    public save() {
        this.saving = true;
        this.dataService.UpdateTeamOwners(this._team.Id, this.users || [], this.feedback.bind(this))
            .then(() => {
                this.saved();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Owners saved";
                this.notify(c);
                this.logService.addMessage("Operation finished successfully", "Info");
            })
            .catch((e) => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.logService.addMessage("Operation cancelled", "Error");
                this.$exceptionHandler(e);
            })
            .finally(() => { this.saving = false; });
    }
    constructor(
        private $q: ng.IQService,
        private $exceptionHandler: ng.IExceptionHandlerService,
        private auth: IAuthenticationService,
        private logService: IFeedbackService,
        private dataService: IDataService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions) {
    }
    public static $inject = ["$q", "$exceptionHandler", "authenticationService", "feedbackService", "dataService", "notify", "notifyOkConfig"];
}
