var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "./ComponentDecorator"], function (require, exports, app_1, ComponentDecorator_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var UserDetailComponent = /** @class */ (function () {
        function UserDetailComponent(auth, dataService) {
            var _this = this;
            this.auth = auth;
            this.dataService = dataService;
            this.loading = false;
            auth.acquireToken().then(function () { _this.loading = true; });
            dataService.GetUsers().then(function (x) {
                _this.users = x;
                _this.loading = false;
            });
        }
        UserDetailComponent.$inject = ["authenticationService", "dataService"];
        UserDetailComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'userDetail', {
                controllerAs: 'ctrl',
                templateUrl: 'app/views/userDetail.html',
                bindings: { userId: "<" }
            })
        ], UserDetailComponent);
        return UserDetailComponent;
    }());
    exports.UserDetailComponent = UserDetailComponent;
});
//# sourceMappingURL=UserDetailComponent.js.map