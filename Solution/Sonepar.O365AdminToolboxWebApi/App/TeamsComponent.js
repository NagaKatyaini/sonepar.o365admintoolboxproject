var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "angular", "./ComponentDecorator", "./Model/OfficeObject"], function (require, exports, app_1, angular, ComponentDecorator_1, OfficeObject_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TeamsComponent = /** @class */ (function () {
        function TeamsComponent($exceptionHandler, scroll, modal, auth, dataService, logService, notify, notifyOkConfig, config) {
            var _this = this;
            this.$exceptionHandler = $exceptionHandler;
            this.scroll = scroll;
            this.modal = modal;
            this.auth = auth;
            this.dataService = dataService;
            this.logService = logService;
            this.notify = notify;
            this.notifyOkConfig = notifyOkConfig;
            this.config = config;
            this.ownerPlaceHolder = 'Loading users...';
            this.saving = false;
            this.loading = false;
            this.isCreateMode = false;
            this.isOwnersMode = false;
            this.isMembersMode = false;
            this.isEditMode = false;
            this.scroll.yOffset = 150;
            auth.acquireToken().then(function () {
                _this.getTeam();
                return dataService.GetUsers().then(function (x) {
                    _this.loading = false;
                    _this.ownerPlaceHolder = "Search for users";
                    _this.users = x;
                });
            }).catch(function (e) { });
            this.dataTeam = new OfficeObject_1.default();
            this.teamNamePlaceholder = config.teamNamePlaceholder;
        }
        Object.defineProperty(TeamsComponent.prototype, "userCountries", {
            get: function () { return this.auth.userCountries; },
            enumerable: true,
            configurable: true
        });
        TeamsComponent.prototype.addOwners = function (users) {
            var _this = this;
            users.forEach(function (u) {
                if (!_this.dataTeam.Owners.some(function (o) { return o.Id == u.Id; })) {
                    _this.dataTeam.Owners.push(u);
                }
            });
        };
        TeamsComponent.prototype.removeOwners = function (users) {
            this.dataTeam.Owners = this.dataTeam.Owners.filter(function (o) { return !users.some(function (u) { return u.Id === o.Id; }); });
        };
        TeamsComponent.prototype.showCreateForm = function () {
            this.scroll("edit");
            this.isCreateMode = true;
            this.isMembersMode = false;
            this.isOwnersMode = false;
            this.isEditMode = false;
            this.selectedTeam = undefined;
        };
        TeamsComponent.prototype.showEditForm = function (team) {
            console.log(team);
            this.scroll("edit");
            this.isCreateMode = false;
            this.isEditMode = true;
            this.isMembersMode = false;
            this.isOwnersMode = false;
            this.selectedTeam = __assign({}, team);
        };
        TeamsComponent.prototype.showMembersForm = function (team) {
            this.scroll("edit");
            this.isMembersMode = true;
            this.isCreateMode = false;
            this.isOwnersMode = false;
            this.isEditMode = false;
            this.selectedTeam = team;
        };
        TeamsComponent.prototype.showOwnersForm = function (team) {
            this.scroll("edit");
            this.isMembersMode = false;
            this.isCreateMode = false;
            this.isOwnersMode = true;
            this.isEditMode = false;
            this.selectedTeam = team;
        };
        TeamsComponent.prototype.hideCreateForm = function () {
            this.dataTeam = new OfficeObject_1.default();
            this.isCreateMode = false;
        };
        TeamsComponent.prototype.hideEditForm = function () {
            this.dataTeam = new OfficeObject_1.default();
            this.isEditMode = false;
        };
        TeamsComponent.prototype.sendTeam = function () {
            var _this = this;
            this.saving = true;
            var name = this.dataTeam.Name;
            this.dataService.PutTeam(this.dataTeam)
                .then(function (x) {
                _this.hideCreateForm();
                _this.getTeam();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Team saved";
                _this.notify(c);
                _this.logService.addMessage("Team " + name + " created", "Trace");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            }).finally(function () { _this.saving = false; });
        };
        TeamsComponent.prototype.saveTeam = function () {
            var _this = this;
            this.saving = true;
            var groupId = this.selectedTeam ? this.selectedTeam.Id : "";
            var groupName = this.selectedTeam ? this.selectedTeam.Name : "";
            var datasToUpdate = {
                Id: groupId,
                Name: groupName,
            };
            console.log("teamName", groupName);
            this.dataService.UpdateTeam(datasToUpdate)
                .then(function (x) {
                _this.hideEditForm();
                _this.getTeam();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Team saved";
                _this.notify(c);
                _this.logService.addMessage("Team " + name + " updated", "Trace");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            }).finally(function () { _this.saving = false; });
        };
        TeamsComponent.prototype.getTeam = function () {
            var _this = this;
            this.loading = true;
            this.dataService.GetTeams().then(function (x) {
                _this.teams = x;
                _this.loading = false;
            });
        };
        TeamsComponent.prototype.isMatchFilter = function (filter) {
            return function (value) { return value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1; };
        };
        TeamsComponent.prototype.matchUser = function (filter) {
            var _this = this;
            return function (u) {
                var isMatch = _this.isMatchFilter(filter);
                return (filter == null
                    || filter.length === 0
                    || isMatch(u.DisplayName)
                    || isMatch(u.City)
                    || isMatch(u.Department)
                    || isMatch(u.Country)
                    || isMatch(u.JobTitle)
                    || isMatch(u.Mail)
                    || isMatch(u.UserPrincipalName));
            };
        };
        TeamsComponent.prototype.tryDelete = function (team) {
            var _this = this;
            var modal = this.modal.open({
                size: 'lg',
                controller: DeleteTeamController,
                controllerAs: "ctrl",
                templateUrl: 'app/views/deleteTeam.html',
                resolve: {
                    "team": team
                }
            });
            modal.closed.catch(function () { });
            modal.result.then(function () { return _this.dataService.DeleteTeam(team); }).then(function () {
                _this.getTeam();
                var notification = angular.copy(_this.notifyOkConfig);
                notification.message = "Team \"" + team.Name + "\" has been successfully deleted";
                _this.notify(notification);
                _this.logService.addMessage("Team \"" + team.Name + "\" has been successfully deleted", "Info");
                _this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.$exceptionHandler(e);
            });
        };
        TeamsComponent.$inject = ["$exceptionHandler", "$anchorScroll", "$uibModal", "authenticationService", "dataService", "feedbackService", "notify", "notifyOkConfig", "config"];
        TeamsComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'teams', {
                controllerAs: 'ctrl',
                templateUrl: 'app/views/teams.html'
            })
        ], TeamsComponent);
        return TeamsComponent;
    }());
    exports.TeamsComponent = TeamsComponent;
    var DeleteTeamController = /** @class */ (function () {
        function DeleteTeamController($uibModalInstance, team) {
            this.$uibModalInstance = $uibModalInstance;
            this.team = team;
        }
        DeleteTeamController.prototype.cancel = function () {
            this.$uibModalInstance.dismiss();
        };
        DeleteTeamController.prototype.ok = function () {
            this.$uibModalInstance.close();
        };
        DeleteTeamController.$inject = ["$uibModalInstance", "team"];
        return DeleteTeamController;
    }());
});
//# sourceMappingURL=TeamsComponent.js.map