var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
define(["require", "exports", "./app", "./ComponentDecorator"], function (require, exports, app_1, ComponentDecorator_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var LoginComponent = /** @class */ (function () {
        function LoginComponent(adal, auth, tokenStore, feedbackService, timeout, $exceptionHandler, config) {
            var _this = this;
            this.adal = adal;
            this.auth = auth;
            this.tokenStore = tokenStore;
            this.feedbackService = feedbackService;
            this.timeout = timeout;
            this.$exceptionHandler = $exceptionHandler;
            this.config = config;
            this.enableTeams = false;
            this.enablePlans = false;
            this.enableWorkspaces = false;
            this.loading = true;
            this.auth.acquireToken().catch(function (e) {
                if (_this.adal.userInfo.isAuthenticated) {
                    $exceptionHandler(e);
                }
            }).finally(function () { _this.loading = false; });
            this.enableTeams = config.enableTeams;
            this.enablePlans = config.enablePlans;
            this.enableWorkspaces = config.enableWorkspaces;
        }
        Object.defineProperty(LoginComponent.prototype, "unreadFeedbacks", {
            get: function () {
                return this.feedbackService.unreadFeedbacks;
            },
            enumerable: true,
            configurable: true
        });
        LoginComponent.prototype.log = function () {
            var vals = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                vals[_i] = arguments[_i];
            }
            console.log.apply(console, __spreadArrays([""], vals));
        };
        Object.defineProperty(LoginComponent.prototype, "UserName", {
            get: function () {
                return this.adal.userInfo.userName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginComponent.prototype, "Profile", {
            get: function () {
                return this.adal.userInfo.profile;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginComponent.prototype, "showLogin", {
            get: function () {
                return !(this.adal.userInfo.isAuthenticated || this.tokenStore.token);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginComponent.prototype, "isLoggedInAad", {
            get: function () {
                return this.adal.userInfo.isAuthenticated;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginComponent.prototype, "isLoggedIn", {
            get: function () {
                return this.adal.userInfo.isAuthenticated && !!this.tokenStore.token;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginComponent.prototype, "isLoading", {
            get: function () {
                return this.loading;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginComponent.prototype, "ready", {
            get: function () {
                return !(this.loading || this.noRight);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LoginComponent.prototype, "noRight", {
            get: function () {
                return this.auth.userCountries.length === 0;
            },
            enumerable: true,
            configurable: true
        });
        ;
        LoginComponent.prototype.login = function () {
            this.adal.login();
        };
        LoginComponent.prototype.logout = function () {
            this.adal.logOut();
            this.tokenStore.clear();
        };
        LoginComponent.prototype.$onDestroy = function () {
            this.feedbackService.unregisterForMessages(this.registration);
        };
        LoginComponent.$inject = ["adalAuthenticationService", "authenticationService", "tokenStore", "feedbackService", "$timeout", "$exceptionHandler", "config"];
        LoginComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'login', {
                controllerAs: 'ctrl',
                templateUrl: 'app/views/login.html'
            })
        ], LoginComponent);
        return LoginComponent;
    }());
    exports.LoginComponent = LoginComponent;
});
//# sourceMappingURL=LoginComponent.js.map