var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "./ComponentDecorator", "angular"], function (require, exports, app_1, ComponentDecorator_1, angular) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var PlanOwnersComponent = /** @class */ (function () {
        function PlanOwnersComponent($q, $exceptionHandler, auth, logService, dataService, notify, notifyOkConfig) {
            this.$q = $q;
            this.$exceptionHandler = $exceptionHandler;
            this.auth = auth;
            this.logService = logService;
            this.dataService = dataService;
            this.notify = notify;
            this.notifyOkConfig = notifyOkConfig;
            this.loading = false;
            this.saving = false;
        }
        Object.defineProperty(PlanOwnersComponent.prototype, "plan", {
            get: function () {
                return this._plan;
            },
            set: function (g) {
                this._plan = g;
                if (g) {
                    this.load();
                }
            },
            enumerable: true,
            configurable: true
        });
        PlanOwnersComponent.prototype.load = function () {
            var _this = this;
            this.users = undefined;
            this.dataService.GetPlanOwners(this._plan.Id).then(function (u) { return _this.users = u; });
        };
        PlanOwnersComponent.prototype.saved = function () {
            this.onSaved && this.onSaved();
        };
        PlanOwnersComponent.prototype.feedback = function (a, member) {
            this.logService.addMessage("Plan " + this.plan.Name + ", owner " + member.DisplayName + " " + a, "Trace");
        };
        PlanOwnersComponent.prototype.save = function () {
            var _this = this;
            this.saving = true;
            this.dataService.UpdatePlanOwners(this._plan.Id, this.users || [], this.feedback.bind(this))
                .then(function () {
                _this.saved();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Owners saved";
                _this.notify(c);
                _this.logService.addMessage("Operation finished successfully", "Info");
            })
                .catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.logService.addMessage("Operation cancelled", "Error");
                _this.$exceptionHandler(e);
            })
                .finally(function () { _this.saving = false; });
        };
        PlanOwnersComponent.$inject = ["$q", "$exceptionHandler", "authenticationService", "feedbackService", "dataService", "notify", "notifyOkConfig"];
        PlanOwnersComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'planowners', {
                controllerAs: 'ctrl',
                bindings: { "plan": "<", onSaved: "&" },
                templateUrl: 'app/views/planowners.html'
            })
        ], PlanOwnersComponent);
        return PlanOwnersComponent;
    }());
    exports.PlanOwnersComponent = PlanOwnersComponent;
});
//# sourceMappingURL=PlanOwnersComponent.js.map