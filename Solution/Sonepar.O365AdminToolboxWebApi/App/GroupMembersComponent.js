var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "./ComponentDecorator", "angular"], function (require, exports, app_1, ComponentDecorator_1, angular) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var GroupMembersComponent = /** @class */ (function () {
        function GroupMembersComponent($q, $exceptionHandler, auth, logService, dataService, notify, notifyOkConfig) {
            this.$q = $q;
            this.$exceptionHandler = $exceptionHandler;
            this.auth = auth;
            this.logService = logService;
            this.dataService = dataService;
            this.notify = notify;
            this.notifyOkConfig = notifyOkConfig;
            this.loading = false;
            this.saving = false;
        }
        Object.defineProperty(GroupMembersComponent.prototype, "group", {
            get: function () {
                return this._group;
            },
            set: function (g) {
                this._group = g;
                if (g) {
                    this.load();
                }
            },
            enumerable: true,
            configurable: true
        });
        GroupMembersComponent.prototype.load = function () {
            var _this = this;
            this.users = undefined;
            this.dataService.GetGroupMembers(this._group.Id).then(function (u) { return _this.users = u; });
        };
        GroupMembersComponent.prototype.saved = function () {
            this.onSaved && this.onSaved();
        };
        GroupMembersComponent.prototype.save = function () {
            var _this = this;
            this.saving = true;
            this.dataService.UpdateGroupMembers(this._group.Id, this.users || [], this.feedback.bind(this))
                .then(function () {
                _this.saved();
                var c = angular.copy(_this.notifyOkConfig);
                c.message = "Members saved";
                _this.notify(c);
                _this.logService.addMessage("Operation finished successfully", "Info");
            })
                .catch(function (e) {
                _this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                _this.logService.addMessage("Operation cancelled", "Error");
                _this.$exceptionHandler(e);
            })
                .finally(function () { _this.saving = false; });
        };
        GroupMembersComponent.prototype.feedback = function (a, member) {
            this.logService.addMessage("Group " + this.group.Name + ", member " + member.DisplayName + " " + a, "Trace");
        };
        GroupMembersComponent.$inject = ["$q", "$exceptionHandler", "authenticationService", "feedbackService", "dataService", "notify", "notifyOkConfig"];
        GroupMembersComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'groupmembers', {
                controllerAs: 'ctrl',
                bindings: { "group": "<", onSaved: "&" },
                templateUrl: 'app/views/groupmembers.html'
            })
        ], GroupMembersComponent);
        return GroupMembersComponent;
    }());
    exports.GroupMembersComponent = GroupMembersComponent;
});
//# sourceMappingURL=GroupMembersComponent.js.map