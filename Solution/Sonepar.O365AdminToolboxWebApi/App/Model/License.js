define(["require", "exports", "./Service"], function (require, exports, Service_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ServiceAssignement = /** @class */ (function () {
        function ServiceAssignement() {
        }
        return ServiceAssignement;
    }());
    exports.ServiceAssignement = ServiceAssignement;
    var License = /** @class */ (function () {
        function License(lic) {
            if (lic) {
                this.Id = lic.Id;
                this.FriendlyName = lic.FriendlyName;
                this.Name = lic.Name;
                this.UsageLocation = lic.UsageLocation;
                this.ConsumedUnits = lic.ConsumedUnits;
                this.PrepaidUnitsEnabled = lic.PrepaidUnitsEnabled;
                this.AppliesTo = lic.AppliesTo;
                this.Services = lic.Services.map(function (x) {
                    var s = new ServiceAssignement();
                    s.IsActive = x.IsActive;
                    s.Service = new Service_1.default(x.Service);
                    return s;
                });
            }
        }
        Object.defineProperty(License.prototype, "sId", {
            get: function () {
                return (this.Id || '').replace(/-/g, '');
            },
            enumerable: true,
            configurable: true
        });
        return License;
    }());
    exports.default = License;
    var LicenseAssignment = /** @class */ (function () {
        function LicenseAssignment(lic, active) {
            if (lic) {
                this.License = new License(lic);
            }
            this.IsActive = active;
        }
        return LicenseAssignment;
    }());
    exports.LicenseAssignment = LicenseAssignment;
});
//# sourceMappingURL=License.js.map