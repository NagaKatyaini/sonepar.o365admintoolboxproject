define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Member = /** @class */ (function () {
        function Member(member) {
            if (member) {
                this.Id = member.Id;
            }
        }
        return Member;
    }());
    exports.default = Member;
});
//# sourceMappingURL=Member.js.map