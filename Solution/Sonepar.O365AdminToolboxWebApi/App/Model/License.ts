﻿import Service from './Service';
import { AppliesTo } from './Service';


export class ServiceAssignement {
    public Service: Service;
    public IsActive: boolean | undefined;
}

export default class License {
    public Id: string | undefined;
    public get sId() {
        return (this.Id||'').replace(/-/g, '');
    }

    public FriendlyName: string;
    public Name: string;
    public UsageLocation: string | undefined;
    public AppliesTo: AppliesTo;
    public ConsumedUnits: number;
    public PrepaidUnitsEnabled: number;
    public Services: ServiceAssignement[];
    public constructor(lic?: License) {
        if (lic) {
            this.Id = lic.Id
            this.FriendlyName = lic.FriendlyName;
            this.Name = lic.Name;
            this.UsageLocation = lic.UsageLocation;
            this.ConsumedUnits = lic.ConsumedUnits
            this.PrepaidUnitsEnabled = lic.PrepaidUnitsEnabled;
            this.AppliesTo = lic.AppliesTo;
            this.Services = lic.Services.map(x => {
                var s = new ServiceAssignement();
                s.IsActive = x.IsActive;
                s.Service = new Service(x.Service);
                return s;
            })
        }
    }
}

export class LicenseAssignment {
    public License: License;
    public IsActive: boolean | undefined;
    public constructor(lic?: License, active?: boolean) {
        if (lic) {
            this.License = new License(lic);
        }
        this.IsActive = active;
    }
}