﻿import Member from './Member'
export default class OfficeObject extends Member {

    public constructor(officeObject?: OfficeObject) {
        super(officeObject)
        if (officeObject) {
            this.Id = officeObject.Id;
            this.Country = officeObject.Country;
            this.Name = officeObject.Name;
            this.Owners = officeObject.Owners;
            this.Visibility = officeObject.Visibility;
            this.Url = officeObject.Url;
        }
        this.Owners = this.Owners || [];
    }

    public Country: string;
    public Name: string;
    public Owners: Member[];
    public Visibility: 'Private' | 'Public' | undefined = undefined;
    public Url: string;
}
