﻿export type AppliesTo = 'User' | 'Company' | undefined;

export default class Service {
    public Id: string;
    public get sId() {
        return this.Id.replace(/-/g, '');
    }
    public FriendlyName: string;
    public Name: string;
    public AppliesTo: AppliesTo; 
    public constructor(serv?: Service) {
        if (serv) {
            this.FriendlyName = serv.FriendlyName;
            this.Id = serv.Id;
            this.Name = serv.Name;
            this.AppliesTo = serv.AppliesTo;
        }
    }
}
