var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./Member"], function (require, exports, Member_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var OfficeObject = /** @class */ (function (_super) {
        __extends(OfficeObject, _super);
        function OfficeObject(officeObject) {
            var _this = _super.call(this, officeObject) || this;
            _this.Visibility = undefined;
            if (officeObject) {
                _this.Id = officeObject.Id;
                _this.Country = officeObject.Country;
                _this.Name = officeObject.Name;
                _this.Owners = officeObject.Owners;
                _this.Visibility = officeObject.Visibility;
                _this.Url = officeObject.Url;
            }
            _this.Owners = _this.Owners || [];
            return _this;
        }
        return OfficeObject;
    }(Member_1.default));
    exports.default = OfficeObject;
});
//# sourceMappingURL=OfficeObject.js.map