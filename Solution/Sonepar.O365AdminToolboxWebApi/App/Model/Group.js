var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./Member"], function (require, exports, Member_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Group = /** @class */ (function (_super) {
        __extends(Group, _super);
        function Group(group) {
            var _this = _super.call(this, group) || this;
            _this.Visibility = undefined; // = 'Private';
            if (group) {
                _this.Country = group.Country;
                _this.Description = group.Description;
                _this.Mail = group.Mail;
                _this.MailNickname = group.MailNickname;
                _this.Name = group.Name;
                _this.Owners = group.Owners;
                _this.Visibility = group.Visibility;
            }
            _this.Owners = _this.Owners || [];
            return _this;
        }
        return Group;
    }(Member_1.default));
    exports.default = Group;
});
//# sourceMappingURL=Group.js.map