var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "./app", "./ComponentDecorator"], function (require, exports, app_1, ComponentDecorator_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var FeedbackComponent = /** @class */ (function () {
        function FeedbackComponent(logservice) {
            var _this = this;
            this.logservice = logservice;
            this.logservice.markRead();
            this.messages = logservice.getMessages();
            this.registration = logservice.registerForMessages(function (message) {
                _this.messages.unshift(message);
                _this.logservice.markRead();
            });
        }
        FeedbackComponent.prototype.$onDestroy = function () {
            this.logservice.unregisterForMessages(this.registration);
        };
        FeedbackComponent.$inject = [
            "feedbackService",
        ];
        FeedbackComponent = __decorate([
            ComponentDecorator_1.default(app_1.app, 'feedback', {
                controllerAs: 'ctrl',
                bindings: {},
                templateUrl: 'app/views/feedback.html'
            })
        ], FeedbackComponent);
        return FeedbackComponent;
    }());
    exports.FeedbackComponent = FeedbackComponent;
});
//# sourceMappingURL=FeedbackComponent.js.map