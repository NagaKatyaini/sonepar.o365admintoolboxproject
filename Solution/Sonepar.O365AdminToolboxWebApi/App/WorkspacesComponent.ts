﻿import { app } from './app';
import * as angular from 'angular';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IFeedbackService } from './Services/FeedbackService';
import { IConfig } from 'interfaces/Config';

import Component from './ComponentDecorator';
import User from './Model/User';
import OfficeObject from './Model/OfficeObject';
import Workspace from './Model/Workspace';

@Component(app, 'workspaces', {
    controllerAs: 'ctrl',
    templateUrl: 'app/views/workspaces.html'
}) export class WorkspacesComponent {
    public get userCountries(): string[] { return this.auth.userCountries; }
    public workspaces: OfficeObject[];
    public users: User[];
    public workspaceOwners: User[];
    public dataWorkspace: Workspace;
    public ownerPlaceHolder: string = 'Loading users...';
    public message: string;
    public saving: boolean = false;
    public loading: boolean = false;
    public isCreateMode: boolean = false;
    public isEditMode: boolean = false;
    public isOwnersMode: boolean = false;
    public selectedWorkspace: OfficeObject | undefined;
    public ownerWorkspaceId: string;
    public workspaceNamePlaceholder: string;
    public languages: any[] = [
        {
            "value": "German",
            "key": 1031
        },
        {
            "value": "French",
            "key": 1036
        },
        {
            "value": "Irish",
            "key": 2108
        },
        {
            "value": "Indonesian",
            "key": 1057
        },
        {
            "value": "Norwegian (Bokmål)",
            "key": 1044
        },
        {
            "value": "Russian",
            "key": 1049
        },
        {
            "value": "Chinese (Simplified)",
            "key": 2052
        },
        {
            "value": "Chinese (Traditional)",
            "key": 1028
        },
        {
            "value": "Hindi",
            "key": 1081
        },
        {
            "value": "Malay",
            "key": 1086
        },
        {
            "value": "Slovenian",
            "key": 1060
        },
        {
            "value": "Danish",
            "key": 1030
        },
        {
            "value": "Basque",
            "key": 1069
        },
        {
            "value": "Finnish",
            "key": 1035
        },
        {
            "value": "Dutch",
            "key": 1043
        },
        {
            "value": "Slovak",
            "key": 1051
        },
        {
            "value": "Azerbaijani",
            "key": 1068
        },
        {
            "value": "Bulgarian",
            "key": 1026
        },
        {
            "value": "Galician",
            "key": 1110
        },
        {
            "value": "Turkish",
            "key": 1055
        },
        {
            "value": "Welsh",
            "key": 1106
        },
        {
            "value": "Croatian",
            "key": 1050
        },
        {
            "value": "Hungarian",
            "key": 1038
        },
        {
            "value": "Korean",
            "key": 1042
        },
        {
            "value": "Lithuanian",
            "key": 1063
        },
        {
            "value": "Macedonian",
            "key": 1071
        },
        {
            "value": "Arabic",
            "key": 1025
        },
        {
            "value": "Japanese",
            "key": 1041
        },
        {
            "value": "Latvian",
            "key": 1062
        },
        {
            "value": "Dari",
            "key": 1164
        },
        {
            "value": "Portuguese (Brazil)",
            "key": 1046
        },
        {
            "value": "Portuguese (Portugal)",
            "key": 2070
        },
        {
            "value": "Serbian (Latin, Serbia)",
            "key": 9242
        },
        {
            "value": "Thai",
            "key": 1054
        },
        {
            "value": "Bosnian (Latin)",
            "key": 5146
        },
        {
            "value": "Czech",
            "key": 1029
        },
        {
            "value": "Spanish",
            "key": 3082
        },
        {
            "value": "Hebrew",
            "key": 1037
        },
        {
            "value": "Polish",
            "key": 1045
        },
        {
            "value": "Serbian (Cyrillic, Serbia)",
            "key": 10266
        },
        {
            "value": "Serbian (Latin)",
            "key": 2074
        },
        {
            "value": "Ukrainian",
            "key": 1058
        },
        {
            "value": "Greek",
            "key": 1032
        },
        {
            "value": "English",
            "key": 1033
        },
        {
            "value": "Estonian",
            "key": 1061
        },
        {
            "value": "Italian",
            "key": 1040
        },
        {
            "value": "Kazakh",
            "key": 1087
        },
        {
            "value": "Swedish",
            "key": 1053
        },
        {
            "value": "Vietnamese",
            "key": 1066
        },
        {
            "value": "Catalan",
            "key": 1027
        },
        {
            "value": "Romanian",
            "key": 1048
        }
    ];
    public timeZoneList: any[] = [
        {
            "Id": 39,
            "Description": "(UTC-12:00) International Date Line West"
        },
        {
            "Id": 95,
            "Description": "(UTC-11:00) Coordinated Universal Time-11"
        },
        {
            "Id": 15,
            "Description": "(UTC-10:00) Hawaii"
        },
        {
            "Id": 14,
            "Description": "(UTC-09:00) Alaska"
        },
        {
            "Id": 78,
            "Description": "(UTC-08:00) Baja California"
        },
        {
            "Id": 13,
            "Description": "(UTC-08:00) Pacific Time (US and Canada)"
        },
        {
            "Id": 38,
            "Description": "(UTC-07:00) Arizona"
        },
        {
            "Id": 77,
            "Description": "(UTC-07:00) Chihuahua, La Paz, Mazatlan"
        },
        {
            "Id": 12,
            "Description": "(UTC-07:00) Mountain Time (US and Canada)"
        },
        {
            "Id": 55,
            "Description": "(UTC-06:00) Central America"
        },
        {
            "Id": 11,
            "Description": "(UTC-06:00) Central Time (US and Canada)"
        },
        {
            "Id": 37,
            "Description": "(UTC-06:00) Guadalajara, Mexico City, Monterrey"
        },
        {
            "Id": 36,
            "Description": "(UTC-06:00) Saskatchewan"
        },
        {
            "Id": 35,
            "Description": "(UTC-05:00) Bogota, Lima, Quito"
        },
        {
            "Id": 10,
            "Description": "(UTC-05:00) Eastern Time (US and Canada)"
        },
        {
            "Id": 34,
            "Description": "(UTC-05:00) Indiana (East)"
        },
        {
            "Id": 88,
            "Description": "(UTC-04:30) Caracas"
        },
        {
            "Id": 91,
            "Description": "(UTC-04:00) Asuncion"
        },
        {
            "Id": 9,
            "Description": "(UTC-04:00) Atlantic Time (Canada)"
        },
        {
            "Id": 81,
            "Description": "(UTC-04:00) Cuiaba"
        },
        {
            "Id": 33,
            "Description": "(UTC-04:00) Georgetown, La Paz, Manaus, San Juan"
        },
        {
            "Id": 65,
            "Description": "(UTC-04:00) Santiago"
        },
        {
            "Id": 28,
            "Description": "(UTC-03:30) Newfoundland"
        },
        {
            "Id": 8,
            "Description": "(UTC-03:00) Brasilia"
        },
        {
            "Id": 85,
            "Description": "(UTC-03:00) Buenos Aires"
        },
        {
            "Id": 32,
            "Description": "(UTC-03:00) Cayenne, Fortaleza"
        },
        {
            "Id": 60,
            "Description": "(UTC-03:00) Greenland"
        },
        {
            "Id": 90,
            "Description": "(UTC-03:00) Montevideo"
        },
        {
            "Id": 103,
            "Description": "(UTC-03:00) Salvador"
        },
        {
            "Id": 96,
            "Description": "(UTC-02:00) Coordinated Universal Time-02"
        },
        {
            "Id": 30,
            "Description": "(UTC-02:00) Mid-Atlantic"
        },
        {
            "Id": 29,
            "Description": "(UTC-01:00) Azores"
        },
        {
            "Id": 53,
            "Description": "(UTC-01:00) Cabo Verde"
        },
        {
            "Id": 86,
            "Description": "(UTC) Casablanca"
        },
        {
            "Id": 93,
            "Description": "(UTC) Coordinated Universal Time"
        },
        {
            "Id": 2,
            "Description": "(UTC) Dublin, Edinburgh, Lisbon, London"
        },
        {
            "Id": 31,
            "Description": "(UTC) Monrovia, Reykjavik"
        },
        {
            "Id": 4,
            "Description": "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna"
        },
        {
            "Id": 6,
            "Description": "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague"
        },
        {
            "Id": 3,
            "Description": "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris"
        },
        {
            "Id": 57,
            "Description": "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb"
        },
        {
            "Id": 69,
            "Description": "(UTC+01:00) West Central Africa"
        },
        {
            "Id": 83,
            "Description": "(UTC+01:00) Windhoek"
        },
        {
            "Id": 79,
            "Description": "(UTC+02:00) Amman"
        },
        {
            "Id": 5,
            "Description": "(UTC+02:00) Athens, Bucharest"
        },
        {
            "Id": 80,
            "Description": "(UTC+02:00) Beirut"
        },
        {
            "Id": 49,
            "Description": "(UTC+02:00) Cairo"
        },
        {
            "Id": 98,
            "Description": "(UTC+02:00) Damascus"
        },
        {
            "Id": 50,
            "Description": "(UTC+02:00) Harare, Pretoria"
        },
        {
            "Id": 59,
            "Description": "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius"
        },
        {
            "Id": 27,
            "Description": "(UTC+02:00) Jerusalem"
        },
        {
            "Id": 7,
            "Description": "(UTC+02:00) Minsk (old)"
        },
        {
            "Id": 104,
            "Description": "(UTC+02:00) E. Europe"
        },
        {
            "Id": 100,
            "Description": "(UTC+02:00) Kaliningrad"
        },
        {
            "Id": 26,
            "Description": "(UTC+03:00) Baghdad"
        },
        {
            "Id": 101,
            "Description": "(UTC+03:00) Istanbul"
        },
        {
            "Id": 74,
            "Description": "(UTC+03:00) Kuwait, Riyadh"
        },
        {
            "Id": 109,
            "Description": "(UTC+03:00) Minsk"
        },
        {
            "Id": 51,
            "Description": "(UTC+03:00) Moscow, St. Petersburg, Volgograd"
        },
        {
            "Id": 56,
            "Description": "(UTC+03:00) Nairobi"
        },
        {
            "Id": 25,
            "Description": "(UTC+03:30) Tehran"
        },
        {
            "Id": 24,
            "Description": "(UTC+04:00) Abu Dhabi, Muscat"
        },
        {
            "Id": 110,
            "Description": "(UTC+04:00) Astrakhan, Ulyanovsk"
        },
        {
            "Id": 54,
            "Description": "(UTC+04:00) Baku"
        },
        {
            "Id": 106,
            "Description": "(UTC+04:00) Izhevsk, Samara"
        },
        {
            "Id": 89,
            "Description": "(UTC+04:00) Port Louis"
        },
        {
            "Id": 82,
            "Description": "(UTC+04:00) Tbilisi"
        },
        {
            "Id": 84,
            "Description": "(UTC+04:00) Yerevan"
        },
        {
            "Id": 48,
            "Description": "(UTC+04:30) Kabul"
        },
        {
            "Id": 58,
            "Description": "(UTC+05:00) Ekaterinburg"
        },
        {
            "Id": 87,
            "Description": "(UTC+05:00) Islamabad, Karachi"
        },
        {
            "Id": 47,
            "Description": "(UTC+05:00) Tashkent"
        },
        {
            "Id": 23,
            "Description": "(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi"
        },
        {
            "Id": 66,
            "Description": "(UTC+05:30) Sri Jayawardenepura"
        },
        {
            "Id": 62,
            "Description": "(UTC+05:45) Kathmandu"
        },
        {
            "Id": 71,
            "Description": "(UTC+06:00) Astana"
        },
        {
            "Id": 102,
            "Description": "(UTC+06:00) Dhaka"
        },
        {
            "Id": 115,
            "Description": "(UTC+06:00) Omsk"
        },
        {
            "Id": 61,
            "Description": "(UTC+06:30) Yangon (Rangoon)"
        },
        {
            "Id": 22,
            "Description": "(UTC+07:00) Bangkok, Hanoi, Jakarta"
        },
        {
            "Id": 111,
            "Description": "(UTC+07:00) Barnaul, Gorno-Altaysk"
        },
        {
            "Id": 64,
            "Description": "(UTC+07:00) Krasnoyarsk"
        },
        {
            "Id": 46,
            "Description": "(UTC+07:00) Novosibirsk"
        },
        {
            "Id": 112,
            "Description": "(UTC+07:00) Tomsk"
        },
        {
            "Id": 45,
            "Description": "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi"
        },
        {
            "Id": 63,
            "Description": "(UTC+08:00) Irkutsk"
        },
        {
            "Id": 21,
            "Description": "(UTC+08:00) Kuala Lumpur, Singapore"
        },
        {
            "Id": 73,
            "Description": "(UTC+08:00) Perth"
        },
        {
            "Id": 75,
            "Description": "(UTC+08:00) Taipei"
        },
        {
            "Id": 94,
            "Description": "(UTC+08:00) Ulaanbaatar"
        },
        {
            "Id": 20,
            "Description": "(UTC+09:00) Osaka, Sapporo, Tokyo"
        },
        {
            "Id": 72,
            "Description": "(UTC+09:00) Seoul"
        },
        {
            "Id": 70,
            "Description": "(UTC+09:00) Yakutsk"
        },
        {
            "Id": 19,
            "Description": "(UTC+09:30) Adelaide"
        },
        {
            "Id": 44,
            "Description": "(UTC+09:30) Darwin"
        },
        {
            "Id": 18,
            "Description": "(UTC+10:00) Brisbane"
        },
        {
            "Id": 76,
            "Description": "(UTC+10:00) Canberra, Melbourne, Sydney"
        },
        {
            "Id": 43,
            "Description": "(UTC+10:00) Guam, Port Moresby"
        },
        {
            "Id": 42,
            "Description": "(UTC+10:00) Hobart"
        },
        {
            "Id": 99,
            "Description": "(UTC+10:00) Magadan"
        },
        {
            "Id": 68,
            "Description": "(UTC+10:00) Vladivostok"
        },
        {
            "Id": 107,
            "Description": "(UTC+11:00) Chokurdakh"
        },
        {
            "Id": 114,
            "Description": "(UTC+11:00) Sakhalin"
        },
        {
            "Id": 41,
            "Description": "(UTC+11:00) Solomon Is., New Caledonia"
        },
        {
            "Id": 108,
            "Description": "(UTC+12:00) Anadyr, Petropavlovsk-Kamchatsky"
        },
        {
            "Id": 17,
            "Description": "(UTC+12:00) Auckland, Wellington"
        },
        {
            "Id": 97,
            "Description": "(UTC+12:00) Coordinated Universal Time+12"
        },
        {
            "Id": 40,
            "Description": "(UTC+12:00) Fiji"
        },
        {
            "Id": 92,
            "Description": "(UTC+12:00) Petropavlovsk-Kamchatsky - Old"
        },
        {
            "Id": 67,
            "Description": "(UTC+13:00) Nuku'alofa"
        },
        {
            "Id": 16,
            "Description": "(UTC+13:00) Samoa"
        }
    ];

    public addOwners(users: User[]) {
        users.forEach(u => {
            if (!this.dataWorkspace.Owners.some(o => o.Id == u.Id)) {
                this.dataWorkspace.Owners.push(u);
            }
        })
    }
    public removeOwners(users: User[]) {
        this.dataWorkspace.Owners = this.dataWorkspace.Owners.filter(o => !users.some(u => u.Id === o.Id));
    }

    public showCreateForm() {
        this.scroll("edit")
        this.isCreateMode = true;
        this.isOwnersMode = false;
        this.isEditMode = false;
        this.selectedWorkspace = undefined;
    }

    public showEditForm(workspace: OfficeObject) {
        console.log(workspace);
        this.scroll("edit")
        this.isCreateMode = false;
        this.isOwnersMode = false;
        this.isEditMode = true;
        this.selectedWorkspace = { ...workspace };
    }

    public showOwnersForm(workspace: Workspace) {
        this.scroll("edit")
        this.isCreateMode = false;
        this.isEditMode = false;
        this.isOwnersMode = true;
        this.selectedWorkspace = workspace;
    }

    public hideEditForm() {
        this.dataWorkspace = new Workspace();
        this.isEditMode = false;
    }

    public hideCreateForm() {
        this.dataWorkspace = new Workspace();
        this.isCreateMode = false;
    }

    public sendWorkspace() {
        this.saving = true;
        this.dataService.PutWorkspace(this.dataWorkspace)
            .then(newWorkspace => {
                this.hideCreateForm();
                this.getWorkspace();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Workspace saved";
                this.notify(c);
                this.logService.addMessage(`Workspace ${newWorkspace.Name} created`, "Trace");
                this.logService.addMessage("Operation finished successfully", "Info");

            }).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e);
            }).finally(() => { this.saving = false })

    }

    public saveWorkspace() {
        this.saving = true;
        let workspaceNewId = this.selectedWorkspace ? this.selectedWorkspace.Id : "";
        let workspaceNewUrl = this.selectedWorkspace ? this.selectedWorkspace.Url : "";
        let workspaceNewName = this.selectedWorkspace ? this.selectedWorkspace.Name : "";

        let datasToUpdate = {
            Url: workspaceNewUrl,
            Name: workspaceNewName
        }
        console.log("datasToUpdate", datasToUpdate);

        this.dataService.UpdateWorkspace(workspaceNewId, datasToUpdate)
            .then(x => {
                this.hideEditForm();
                this.getWorkspace();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Workspace saved";
                this.notify(c);
                this.logService.addMessage(`Workspace ${name} updated`, "Trace");
                this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e);
            }).finally(() => { this.saving = false })
    }

    public getWorkspace() {
        this.loading = true;
        this.dataService.GetWorkspaces().then(x => {
            console.log(x);
            this.workspaces = x;
            // this.workspaces.forEach(w => w.Url = this.config.sharePointBaseUrl + '/' + encodeURIComponent(w.Name));
            this.loading = false;
        });
    }

    private isMatchFilter(filter: string) {

        return (value: string) => value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1;
    }
    public matchUser(filter: string) {
        return (u: User) => {
            var isMatch = this.isMatchFilter(filter);
            return (filter == null
                || filter.length === 0
                || isMatch(u.DisplayName)
                || isMatch(u.City)
                || isMatch(u.Department)
                || isMatch(u.Country)
                || isMatch(u.JobTitle)
                || isMatch(u.Mail)
                || isMatch(u.UserPrincipalName));
        };
    }

    public tryDelete(workspace: OfficeObject) {
        var modal = this.modal.open({
            size: 'lg',
            controller: DeleteWorkspaceController,
            controllerAs: "ctrl",
            templateUrl: 'app/views/deleteWorkspace.html',
            resolve: {
                "workspace": workspace
            }
        });
        modal.closed.catch(() => { });
        modal.result.then(() => this.dataService.DeleteWorkspace(workspace)
        ).then(
            () => {
                this.getWorkspace();
                var notification = angular.copy(this.notifyOkConfig);
                notification.message = `Workspace "${workspace.Name}" has been successfully deleted`
                this.notify(notification);
                this.logService.addMessage(`Workspace "${workspace.Name}" has been successfully deleted`, "Info");
                this.logService.addMessage("Operation finished successfully", "Info");
            }
            ).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e)
            });
    }

    constructor(
        private $exceptionHandler: ng.IExceptionHandlerService,
        private scroll: ng.IAnchorScrollService,
        private modal: ng.ui.bootstrap.IModalService,
        private auth: IAuthenticationService,
        private dataService: IDataService,
        private logService: IFeedbackService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions,
        private config: IConfig
    ) {
        this.scroll.yOffset = 150;
        auth.acquireToken().then(() => {
            this.getWorkspace();
            return dataService.GetUsers().then(x => {
                this.loading = false;
                this.ownerPlaceHolder = "Search for users"
                this.users = x;
            });
        }).catch(e => { });
        this.dataWorkspace = new Workspace();
        this.workspaceNamePlaceholder = config.workspaceNamePlaceholder;
    }

    public static $inject = ["$exceptionHandler", "$anchorScroll", "$uibModal", "authenticationService", "dataService", "feedbackService", "notify", "notifyOkConfig", "config"];
}

class DeleteWorkspaceController {
    constructor(
        private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, private workspace: OfficeObject
    ) {

    }
    public cancel() {
        this.$uibModalInstance.dismiss();
    }
    public ok() {
        this.$uibModalInstance.close();
    }

    public static $inject = ["$uibModalInstance", "workspace"];

}