﻿export interface INotifyOptions {
    /**
     * Required. The message to show.
     */
    message: string;

    /**
     * Optional. A custom template for the UI of the message.
     */
    templateUrl ?: string;

    /**
     * Optional. A list of custom CSS classes to apply to the message element.
     */
    classes ?: string;

    /**
     * Optional. A string containing any valid Angular HTML which will be shown instead of the regular message text.
     * The string must contain one root element like all valid Angular HTML templates (so wrap everything in a <span>).
     */
    messageTemplate ?: string;

    /**
     * Optional. A valid Angular scope object. The scope of the template will be created by calling $new() on this scope.
     */
    $scope ?: ng.IScope;

    /**
     * Optional. Currently center and right are the only acceptable values.
     */
    position ?: string;

    /**
     * Optional. The duration (in milliseconds) of the message. A duration of 0 will prevent the message from closing automatically.
     */
    duration ?: number;

    /**
     * Optional. Element that contains each notification. Defaults to document.body.
     */
    container ?: any;
}