﻿import { app } from './app';
import Component from './ComponentDecorator';
import { IAuthenticationService } from 'services/AuthenticationService'

@Component(app, 'app', {
    controllerAs: 'ctrl',
    templateUrl: 'app/views/app.html'
}) export class AppComponent {
    public get isMain(): boolean {
        return this.$window.parent === this.$window.self;
    }
    public get show(): boolean {
        return this.auth.userCountries.length > 0;
    }
    constructor(
        private $window: ng.IWindowService,
        private auth: IAuthenticationService
    ) {
    }
    public static $inject = ["$window","authenticationService"]
}
//app.component('appComponent', AppComponent);