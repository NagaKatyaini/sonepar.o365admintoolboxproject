﻿import { app } from './app';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import Country from './Model/Country';
import Component from './ComponentDecorator';
import OfficeObject from './Model/OfficeObject';
import User from './Model/User';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IFeedbackService } from './Services/FeedbackService';

import * as angular from 'angular';

@Component(app, 'workspaceowners', {
    controllerAs: 'ctrl',
    bindings: { "workspace": "<", onSaved: "&" },
    templateUrl: 'app/views/workspaceowners.html'
}) export class WorkspaceOwnersComponent {
    public users: User[] | undefined;
    private _workspace: OfficeObject;
    public onSaved: () => void;
    public get workspace(): OfficeObject {
        return this._workspace;
    }
    public set workspace(g: OfficeObject) {
        this._workspace = g;
        console.log(g);
        if (g) {
            this.load();
        }
    }
    public loading: boolean = false;
    public saving: boolean = false;
    public filter: string;

    private load() {
        this.users = undefined;
        this.dataService.GetWorkspaceOwners(this._workspace.Id).then((u) => this.users = u);
    }
    private saved() {
        this.onSaved && this.onSaved();
    }
    private feedback(a: string, member: User) {
        this.logService.addMessage(`Workspace ${this.workspace.Name}, owner ${member.DisplayName} ${a}`, "Trace");
    }
    public save() {
        this.saving = true;
        this.dataService.UpdateWorkspaceOwners(this._workspace.Id, this.users || [], this.feedback.bind(this))
            .then(() => {
                this.saved();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Owners saved";
                this.notify(c);
                this.logService.addMessage("Operation finished successfully", "Info");
            })
            .catch((e) => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.logService.addMessage("Operation cancelled", "Error");
                this.$exceptionHandler(e);
            })
            .finally(() => { this.saving = false; });
    }
    constructor(
        private $q: ng.IQService,
        private $exceptionHandler: ng.IExceptionHandlerService,
        private auth: IAuthenticationService,
        private logService: IFeedbackService,
        private dataService: IDataService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions) {
    }
    public static $inject = ["$q", "$exceptionHandler", "authenticationService", "feedbackService", "dataService", "notify", "notifyOkConfig"];
}
