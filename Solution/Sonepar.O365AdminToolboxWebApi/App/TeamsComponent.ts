﻿import { app } from './app';
import * as angular from 'angular';
import { IAuthenticationService } from './Services/AuthenticationService';
import { IDataService } from './Services/DataService';
import { INotifyOptions } from 'interfaces/INotifyOptions';
import { IFeedbackService } from './Services/FeedbackService';
import { IConfig } from 'interfaces/Config';

import Component from './ComponentDecorator';
import User from './Model/User';
import OfficeObject from './Model/OfficeObject';

@Component(app, 'teams', {
    controllerAs: 'ctrl',
    templateUrl: 'app/views/teams.html'
}) export class TeamsComponent {
    public get userCountries(): string[] { return this.auth.userCountries; }
    public teams: OfficeObject[];
    public users: User[];
    public teamOwners: User[];
    public dataTeam: OfficeObject;
    public ownerPlaceHolder: string = 'Loading users...';
    public message: string;
    public saving: boolean = false;
    public loading: boolean = false;
    public isCreateMode: boolean = false;
    public isOwnersMode: boolean = false;
    public isMembersMode: boolean = false;
    public isEditMode: boolean = false;
    public selectedTeam: OfficeObject | undefined;
    public ownerTeamId: string;
    public teamNamePlaceholder: string;

    public addOwners(users: User[]) {
        users.forEach(u => {
            if (!this.dataTeam.Owners.some(o => o.Id == u.Id)) {
                this.dataTeam.Owners.push(u);
            }
        })
    }
    public removeOwners(users: User[]) {
        this.dataTeam.Owners = this.dataTeam.Owners.filter(o => !users.some(u => u.Id === o.Id));
    }

    public showCreateForm() {
        this.scroll("edit")
        this.isCreateMode = true;
        this.isMembersMode = false;
        this.isOwnersMode = false;
        this.isEditMode = false;
        this.selectedTeam = undefined;
    }

    public showEditForm(team: OfficeObject) {
        console.log(team);
        this.scroll("edit")
        this.isCreateMode = false;
        this.isEditMode = true;
        this.isMembersMode = false;
        this.isOwnersMode = false;
        this.selectedTeam = { ...team };
    }

    public showMembersForm(team: OfficeObject) {
        this.scroll("edit")
        this.isMembersMode = true;
        this.isCreateMode = false;
        this.isOwnersMode = false;
        this.isEditMode = false;
        this.selectedTeam = team;
    }
    public showOwnersForm(team: OfficeObject) {
        this.scroll("edit")
        this.isMembersMode = false;
        this.isCreateMode = false;
        this.isOwnersMode = true;
        this.isEditMode = false;
        this.selectedTeam = team;
    }

    public hideCreateForm() {
        this.dataTeam = new OfficeObject();
        this.isCreateMode = false;
    }

    public hideEditForm() {
        this.dataTeam = new OfficeObject();
        this.isEditMode = false;
    }

    public sendTeam() {
        this.saving = true;
        var name = this.dataTeam.Name;
        this.dataService.PutTeam(this.dataTeam)
            .then(x => {
                this.hideCreateForm();
                this.getTeam();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Team saved";
                this.notify(c);
                this.logService.addMessage(`Team ${name} created`, "Trace");
                this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e);
            }).finally(() => { this.saving = false })

    }

    public saveTeam() {
        this.saving = true;
        let groupId = this.selectedTeam ? this.selectedTeam.Id : "";
        let groupName = this.selectedTeam ? this.selectedTeam.Name : "";
        let datasToUpdate = {
            Id: groupId,
            Name: groupName,
        }
        console.log("teamName", groupName);

        this.dataService.UpdateTeam(datasToUpdate)
            .then(x => {
                this.hideEditForm();
                this.getTeam();
                var c = angular.copy(this.notifyOkConfig);
                c.message = "Team saved";
                this.notify(c);
                this.logService.addMessage(`Team ${name} updated`, "Trace");
                this.logService.addMessage("Operation finished successfully", "Info");
            }).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e);
            }).finally(() => { this.saving = false })
    }

    public getTeam() {
        this.loading = true;
        this.dataService.GetTeams().then(x => {
            this.teams = x;
            this.loading = false;
        });
    }

    private isMatchFilter(filter: string) {

        return (value: string) => value != null && value.toLowerCase().indexOf(filter.toLowerCase()) > -1;
    }
    public matchUser(filter: string) {
        return (u: User) => {
            var isMatch = this.isMatchFilter(filter);
            return (filter == null
                || filter.length === 0
                || isMatch(u.DisplayName)
                || isMatch(u.City)
                || isMatch(u.Department)
                || isMatch(u.Country)
                || isMatch(u.JobTitle)
                || isMatch(u.Mail)
                || isMatch(u.UserPrincipalName));
        };
    }

    public tryDelete(team: OfficeObject) {
        var modal = this.modal.open({
            size: 'lg',
            controller: DeleteTeamController,
            controllerAs: "ctrl",
            templateUrl: 'app/views/deleteTeam.html',
            resolve: {
                "team": team
            }
        });
        modal.closed.catch(() => { });
        modal.result.then(() => this.dataService.DeleteTeam(team)
        ).then(
            () => {
                this.getTeam();
                var notification = angular.copy(this.notifyOkConfig);
                notification.message = `Team "${team.Name}" has been successfully deleted`
                this.notify(notification);
                this.logService.addMessage(`Team "${team.Name}" has been successfully deleted`, "Info");
                this.logService.addMessage("Operation finished successfully", "Info");
            }
            ).catch(e => {
                this.logService.addMessage(e && (e.data || e.ExceptionMessage || e.Message) || e, "Error");
                this.$exceptionHandler(e)
            });
    }

    constructor(
        private $exceptionHandler: ng.IExceptionHandlerService,
        private scroll: ng.IAnchorScrollService,
        private modal: ng.ui.bootstrap.IModalService,
        private auth: IAuthenticationService,
        private dataService: IDataService,
        private logService: IFeedbackService,
        private notify: ng.cgNotify.INotifyService,
        private notifyOkConfig: INotifyOptions,
        private config: IConfig
    ) {
        this.scroll.yOffset = 150;
        auth.acquireToken().then(() => {
            this.getTeam();
            return dataService.GetUsers().then(x => {
                this.loading = false;
                this.ownerPlaceHolder = "Search for users"
                this.users = x;
            });
        }).catch(e => { });
        this.dataTeam = new OfficeObject();
        this.teamNamePlaceholder = config.teamNamePlaceholder;
    }

    public static $inject = ["$exceptionHandler", "$anchorScroll", "$uibModal", "authenticationService", "dataService", "feedbackService", "notify", "notifyOkConfig", "config"];
}

class DeleteTeamController {
    constructor(
        private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, private team: OfficeObject
    ) {

    }
    public cancel() {
        this.$uibModalInstance.dismiss();
    }
    public ok() {
        this.$uibModalInstance.close();
    }

    public static $inject = ["$uibModalInstance", "team"];

}