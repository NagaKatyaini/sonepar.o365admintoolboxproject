define(["require", "exports", "../app", "../model/Token", "angular", "adal"], function (require, exports, app_1, Token_1, angular) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var AuthenticationService = /** @class */ (function () {
        function AuthenticationService($q, $http, $rootScope, adal, config, tokenStore) {
            this.$q = $q;
            this.$http = $http;
            this.$rootScope = $rootScope;
            this.adal = adal;
            this.config = config;
            this.tokenStore = tokenStore;
            this._userCountries = [];
        }
        Object.defineProperty(AuthenticationService.prototype, "userCountries", {
            get: function () { return this._userCountries.map(function (x) { return x; }); },
            enumerable: true,
            configurable: true
        });
        AuthenticationService.prototype.acquireToken = function () {
            var _this = this;
            if (!this.promise) {
                var prom = this.$q.when({});
                if (!this.adal.userInfo.isAuthenticated) {
                    prom = this.adal.getUser();
                }
                if (!this.tokenStore.token) {
                    this.promise = prom.then(function () {
                        var defered = _this.$q.defer();
                        var promAcquire = defered.promise;
                        if (!_this.adal.userInfo.isAuthenticated) {
                            _this.$rootScope.$on('adal:loginSuccess', function (event, token) {
                                if (token) {
                                    defered.resolve(token);
                                }
                                else {
                                    defered.reject(new Error("no token"));
                                }
                            });
                        }
                        else {
                            promAcquire = _this.adal.acquireToken(_this.config.clientId);
                        }
                        return promAcquire
                            .then(function (x) {
                            return _this.$http.get(_this.config.BaseServiceUrl + 'api/authentication', { headers: { "Authorization": "Bearer " + x } });
                        }).then(function (x) {
                            _this.tokenStore.token = new Token_1.default(x.data);
                            _this._userCountries = _this.getCountries(_this.tokenStore.token.access_token);
                            return _this.tokenStore.token.access_token;
                        }).catch(function (e) { console.error('acquireToken', e); throw e && (e.Message || e); });
                    });
                }
                else {
                    this.promise = this.$q.when(this.tokenStore.token.access_token);
                }
            }
            return this.promise;
        };
        AuthenticationService.prototype.getCountries = function (token) {
            var countries = [];
            if (token) {
                var elems = token.split(".");
                if (elems.length > 2) {
                    var c = JSON.parse(atob(elems[1]))["https://governance.sonepar.com/claims/adminOf"];
                    if (angular.isArray(c)) {
                        countries = c;
                    }
                    else if (angular.isString(c)) {
                        countries = [c];
                    }
                }
            }
            return countries;
        };
        AuthenticationService.$inject = ["$q", "$http", "$rootScope", "adalAuthenticationService", "config", "tokenStore"];
        return AuthenticationService;
    }());
    exports.AuthenticationService = AuthenticationService;
    app_1.app.service("authenticationService", AuthenticationService);
});
//# sourceMappingURL=AuthenticationService.js.map