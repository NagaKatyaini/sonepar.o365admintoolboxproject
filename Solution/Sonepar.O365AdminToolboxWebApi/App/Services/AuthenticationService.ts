﻿import { app } from '../app'
import { IConfig } from '../Interfaces/Config'
import { ITokenStore } from '../Services/TokenStore'
import TokenResult from '../model/Token'
import 'adal';
import * as angular from 'angular';

export interface IAuthenticationService {
    acquireToken(): ng.IPromise<string>;
    userCountries: string[];
}

export class AuthenticationService implements IAuthenticationService {
    public static $inject = ["$q", "$http", "$rootScope", "adalAuthenticationService", "config", "tokenStore"];
    constructor(
        private $q: ng.IQService,
        private $http: ng.IHttpService,
        private $rootScope: ng.IRootScopeService,
        private adal: adal.AdalAuthenticationService,
        private config: IConfig,
        private tokenStore: ITokenStore) {
    }
    private promise: ng.IPromise<string> | undefined;
    public get userCountries(): string[] { return this._userCountries.map(x => x); }
    private _userCountries: string[] = [];
    public acquireToken(): ng.IPromise<string> {
        if (!this.promise) {
            var prom: ng.IPromise<any> = this.$q.when({});
            if (!this.adal.userInfo.isAuthenticated) {
                prom = this.adal.getUser();
            }
            if (!this.tokenStore.token) {

                this.promise = prom.then(() => {

                    var defered = this.$q.defer();
                    var promAcquire = defered.promise;
                    if (!this.adal.userInfo.isAuthenticated) {
                        this.$rootScope.$on('adal:loginSuccess', function (event, token) {
                            if (token) {
                                defered.resolve(token);
                            }
                            else {
                                defered.reject(new Error("no token"));
                            }
                        });
                    } else {
                        promAcquire = this.adal.acquireToken(this.config.clientId);
                    }
                    return promAcquire
                        .then(x =>
                            this.$http.get<TokenResult>(this.config.BaseServiceUrl + 'api/authentication', { headers: { "Authorization": `Bearer ${x}` } })
                        ).then(x => {
                            this.tokenStore.token = new TokenResult(x.data);
                            this._userCountries = this.getCountries(this.tokenStore.token.access_token);
                            return this.tokenStore.token.access_token;
                        }).catch(e => { console.error('acquireToken', e); throw e && (e.Message || e); });
                });
            }
            else {
                this.promise = this.$q.when(this.tokenStore.token.access_token);
            }
        }

        return this.promise;
    }
    private getCountries(token: string): string[] {
        var countries = [];
        if (token) {
            var elems = token.split(".");
            if (elems.length > 2) {
                var c: any = JSON.parse(atob(elems[1]))["https://governance.sonepar.com/claims/adminOf"];
                if (angular.isArray(c)) {
                    countries = c
                } else if (angular.isString(c)) {
                    countries = [c];
                }
            }
        }
        return countries;
    }
}

app.service("authenticationService", AuthenticationService);
