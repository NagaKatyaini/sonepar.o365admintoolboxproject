import { app } from '../app';
import Country from '../Model/Country';
import Group from '../Model/Group';
import OfficeObject from '../Model/OfficeObject';
import User from '../Model/User';
import License from '../Model/License';
import { LicenseAssignment } from '../Model/License';
import { IConfig } from '../Interfaces/Config';
import { IAuthenticationService } from '../Services/AuthenticationService';
import 'adal';

export interface IDataService {
    GetUsers(refresh?: boolean): ng.IPromise<User[]>;
    GetGroupMembersByPage(userPayload: any): ng.IPromise<User[]>;
    GetGroupAllMembers(grpId: string): ng.IPromise<User[]>; 
    GetUserGroups(refresh?: boolean): ng.IPromise<any[]>;
    GetLicenses(): ng.IPromise<License[]>;
    GetLicensesNames(): ng.IPromise<any[]>;
    GetCountries(): ng.IPromise<Country[]>;
    SaveLicenses(users: User[], Licenses: LicenseAssignment[], cb: (action: string, user: User) => void): ng.IPromise<any>;
    UpdateLicencesNames(licenceNameList: any[]): ng.IPromise<any>;

    GetAllUsers(): ng.IPromise<User[]>;
    GetUser(id: string): ng.IPromise<User>;    
    UpdateUserStatut(user: User, statut: boolean): ng.IPromise<boolean>;
    GetUserMFAStatut(user: User): ng.IPromise<any>;
    ResetMFA(user: User): ng.IPromise<any>;
    UpdateUserMFAStatut(user: User, statut: string): ng.IPromise<any>;

    GetGroups(): ng.IPromise<Group[]>;
    GetUnregisteredGroups(): ng.IPromise<Group[]>;
    DeleteGroup(group: Group): ng.IPromise<void>;
    UpdateGroup(datasToUpdate: any): ng.IPromise<Group>
    PutGroup(data: Group): ng.IPromise<string>;
    GetGroupOwners(id: string): ng.IPromise<User[]>;
    UpdateGroupOwners(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void>;
    GetGroupMembers(id: string): ng.IPromise<User[]>;
    UpdateGroupMembers(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void>;

    GetTeams(): ng.IPromise<OfficeObject[]>;
    UpdateTeam(teamInfos: any): ng.IPromise<OfficeObject>;
    DeleteTeam(team: OfficeObject): ng.IPromise<void>;
    PutTeam(data: OfficeObject): ng.IPromise<string>;
    GetTeamOwners(id: string): ng.IPromise<User[]>;
    UpdateTeamOwners(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void>;
    GetTeamMembers(id: string): ng.IPromise<User[]>;
    UpdateTeamMembers(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void>;

    GetPlans(): ng.IPromise<OfficeObject[]>;
    UpdatePlan(planInfos: any): ng.IPromise<OfficeObject>;
    DeletePlan(plan: OfficeObject): ng.IPromise<void>;
    PutPlan(data: OfficeObject): ng.IPromise<string>;
    GetPlanOwners(id: string): ng.IPromise<User[]>;
    UpdatePlanOwners(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void>;
    GetPlanMembers(id: string): ng.IPromise<User[]>;
    UpdatePlanMembers(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void>;

    GetWorkspaces(): ng.IPromise<OfficeObject[]>;
    GetUnregisteredWorkspaces(): ng.IPromise<OfficeObject[]>;
    DeleteWorkspace(workspace: OfficeObject): ng.IPromise<void>;
    UpdateWorkspace(workspaceId: string, datas: any): ng.IPromise<OfficeObject>;
    CreateTeamSite(data: OfficeObject): ng.IPromise<OfficeObject>;
    AddWorkspaceOwners(data: OfficeObject): ng.IPromise<OfficeObject>;
    PutWorkspace(data: OfficeObject): ng.IPromise<OfficeObject>;
    GetWorkspaceOwners(id: string): ng.IPromise<User[]>;
    UpdateWorkspaceOwners(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void>;

    IsCurrentUserLicenceManager(): ng.IPromise<boolean>;
}

class DataService implements IDataService {
    public static $inject = ["$q", "$http", "$timeout", "config", "authenticationService"];
    constructor(
        private $q: ng.IQService,
        private $http: ng.IHttpService,
        private $timeout: ng.ITimeoutService,
        private config: IConfig,
        private auth: IAuthenticationService) {
    }

    private cacheDate: Date = new Date(0);
    private usersProm: ng.IPromise<User[]>
    public GetUsers(refresh: boolean = false): ng.IPromise<any[]> {
        var now = new Date();
        var cacheExpirationInMs = this.config.cacheExpiration * 1000;
        var expired = (now.getTime() - this.cacheDate.getTime() > cacheExpirationInMs);

        if (!this.usersProm || refresh || expired) {
            this.usersProm = this.auth.acquireToken().then(
                x => {
                    this.cacheDate = now;
                    return this.$http.get<User[]>(this.config.ApiBaseUrl + 'user');
                }
            ).then(x =>
                (x.data || []));
        }
        return this.usersProm;
    }

    private userGroupsProm: ng.IPromise<any[]>
    public GetUserGroups(refresh: boolean = false): ng.IPromise<any[]> {
        var now = new Date();
        var cacheExpirationInMs = this.config.cacheExpiration * 1000;
        var expired = (now.getTime() - this.cacheDate.getTime() > cacheExpirationInMs);
        if (!this.usersProm || refresh || expired) {
            this.userGroupsProm = this.auth.acquireToken().then(
                x => {
                    this.cacheDate = now;
                    return this.$http.get<Group[]>(this.config.ApiBaseUrl + 'user/groups');
                }
            ).then(x => {
                console.log(x);
                return x.data || []
            }
               
            );
        }
        return this.userGroupsProm;
    }
    //TODO Comment
    public GetAllUsers(): ng.IPromise<User[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User[]>(this.config.ApiBaseUrl + 'Users')
        ).then(x =>
            x.data);
    }

    public UpdateUserStatut(user: User, statut: boolean): ng.IPromise<boolean> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<User>(this.config.ApiBaseUrl + `user/${user.Id}/statut`, statut)
        ).then(x =>
            true
        ).catch(e => { return false });
    }

    public ResetMFA(user: User): ng.IPromise<any> {
        let payload = {
            userPrincipalName: user.UserPrincipalName
        }
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<User>(this.config.ApiBaseUrl + `user/resetmfa`, payload)
        ).then(x =>
            x.data
        ).catch(e => { return false });       
    }

    public GetUserMFAStatut(user: User): ng.IPromise<any> {
        console.log('GetUserMFAStatut', user)
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<any>(this.config.ApiBaseUrl + `User/${user.UserPrincipalName}/mfastatus`)
        ).then(x =>
            x.data
        ).catch(e => { return false });
    }

    public UpdateUserMFAStatut(user: User, statut: string): ng.IPromise<any> {
        console.log(user, statut);
        let payload = {
            userStatut: statut,
            userPrincipalName: user.Mail
        }
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<User>(this.config.ApiBaseUrl + `user/mfastatut`, payload)
        ).then(x =>
            x.data
        ).catch(e => { return false });
    }

    public IsCurrentUserLicenceManager(): ng.IPromise<boolean> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<boolean>(this.config.ApiBaseUrl + 'user/iscurrentuserlicencemanager')
        ).then(x =>
            x.data);
    }

    public GetUser(id: string): ng.IPromise<User> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User>(this.config.ApiBaseUrl + `user/${id}`, { cache: false })
        ).then(x =>
            new User(x.data)
            );
    }
   


    public SetUserLicense(user: User) {

    }
    public GetCountries(): ng.IPromise<Country[]> {
        return this.$http.get<Country[]>(this.config.BaseServiceUrl + "Content/countries.json")
            .then(x => x.data);
    }
    public GetLicenses(): ng.IPromise<License[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<License[]>(this.config.ApiBaseUrl + 'ServicePlan')
        ).then(x =>
            x.data);
    }

    public GetLicensesNames(): ng.IPromise<any[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<License[]>(this.config.ApiBaseUrl + 'ServicePlan/friendlynames')
        ).then(x =>
            x.data);
    }

    public UpdateLicencesNames(licenceNameList: any[]): ng.IPromise<any> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<any[]>(this.config.ApiBaseUrl + 'ServicePlan/update', licenceNameList)
        ).then(x => x);
    }

    public SaveLicenses(users: User[], Licenses: LicenseAssignment[], cb?: (action: string, user: User, licName: string) => void): ng.IPromise<any> {
        var toAdd: { user: User, license: License }[] = [];
        var toUpd: { user: User, license: License }[] = [];
        var toRemove: { user: User, license: License }[] = [];

        users.forEach(u => {
            Licenses.forEach(l => {
                var prevActive = u.AssignedLicenses.filter(al => al.Id === l.License.Id)[0];
                if (prevActive && !this.defaultVal(true, l.IsActive) && l.License.Id) {
                    toRemove.push({ user: u, license: l.License });
                } else if (!prevActive && this.defaultVal(false, l.IsActive)) {
                    var nl = new License(l.License);
                    nl.Services.forEach(s => s.IsActive = this.defaultVal(true, s.IsActive))
                    toAdd.push({ user: u, license: nl });
                } else if (prevActive && l.IsActive) {
                    var nl = new License(prevActive);
                    nl.Services.forEach(s => {
                        var ns = l.License.Services.filter(ss => s.Service.Id === ss.Service.Id)[0];
                        s.IsActive = this.defaultVal(this.defaultVal(true, s.IsActive), ns && ns.IsActive);
                    })
                    var changed = nl.Services.some(s => {
                        var ns = prevActive.Services.filter(ss => s.Service.Id === ss.Service.Id)[0];
                        return !ns || ns.IsActive != s.IsActive;
                    });
                    if (changed) {
                        toUpd.push({ user: u, license: nl });
                    }
                }
            });
            u.AssignedLicenses.forEach(l => {
                l.Services.map(s => s.IsActive)
            })
        })
        var promsUpd = toUpd.map(r =>
            ({ u: r.user, f: () => this.$http.post(this.config.ApiBaseUrl + `ServicePlan/${r.user.Id}/plans`, r.license).then(() => { cb && cb('updated', r.user, r.license.FriendlyName); }) })
        );
        var promsDel = toRemove.map(r =>
            ({ u: r.user, f: () => this.$http.delete(this.config.ApiBaseUrl + `ServicePlan/${r.user.Id}/plans/${r.license.Id}`).then(() => cb && cb('removed', r.user, r.license.FriendlyName)) })
        );
        var promsAdd = toAdd.map(r =>
            ({ u: r.user, f: () => this.$http.post(this.config.ApiBaseUrl + `ServicePlan/${r.user.Id}/plans`, r.license).then(() => cb && cb('added', r.user, r.license.FriendlyName)) })
        );

        var proms = promsDel.concat(promsUpd).concat(promsAdd)
        var prom: ng.IPromise<any> = this.$q.when(0);
        var prev: User | null = null;

        for (var p of proms) {
            if (prev && prev == p.u) {
                prom = prom.then(() => this.$timeout(1000))
            }
            prom = prom.then(p.f).catch(e => {
                throw new Error(`operation failed for user ${p.u.DisplayName} : ${e && (e.data || e.ExceptionMessage || e.Message) || e}`);
            })
        }
        if (proms.length === 0) {
            return this.$q.reject("Nothing to update");
        }
        return prom;


    }

    public GetUnregisteredGroups(): ng.IPromise<Group[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<Group[]>(this.config.ApiBaseUrl + 'unregisteredgroups')
        ).then(x =>
            x.data);
    }

    public GetGroups(): ng.IPromise<Group[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<Group[]>(this.config.ApiBaseUrl + 'group')
        ).then(x =>
            x.data);
    }

    public UpdateGroup(datasToUpdate: any): ng.IPromise<Group> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<Group>(this.config.ApiBaseUrl + `group/update`, JSON.stringify(datasToUpdate))
        ).then(x =>
            x.data);
    }
    public DeleteGroup(group: Group): ng.IPromise<void> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.delete<void>(this.config.ApiBaseUrl + `group/${group.Id}`)
        ).then(x => { });
    }
    public PutGroup(data: Group): ng.IPromise<string> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<string>(this.config.ApiBaseUrl + 'group', JSON.stringify(data))
        ).then(x =>
            x.statusText);
    }
    public GetGroupOwners(id: string): ng.IPromise<User[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User[]>(this.config.ApiBaseUrl + `group/${id}/owners`)
        ).then(x =>
            x.data);
    }
    public UpdateGroupOwners(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void> {
        var toAdd: User[] = [];
        var toRemove: User[] = [];
        users = users || [];
        return this.GetGroupOwners(id).then(oldMembers => {
            toRemove = oldMembers.filter(om => !users.some(u => u.Id === om.Id));
            toAdd = users.filter(u => !oldMembers.some(om => u.Id === om.Id));
        }).then(() => this.$q.all(toAdd.map(a =>
            this.$http.put(this.config.ApiBaseUrl + `group/${id}/owners`, a).then(() => cb && cb('added', a))
        ))).then(() => this.$q.all(toRemove.map(r =>
            this.$http.delete(this.config.ApiBaseUrl + `group/${id}/owners/${r.Id}`).then(() => cb && cb('removed', r))
        ))).then(() => { })
    }
    public GetGroupMembers(id: string): ng.IPromise<User[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User[]>(this.config.ApiBaseUrl + `group/${id}/users`)
        ).then(x =>
            x.data);
    }


    public UpdateGroupMembers(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void> {
        var toAdd: User[] = [];
        var toRemove: User[] = [];
        users = users || [];
        return this.GetGroupMembers(id).then(oldMembers => {
            toRemove = oldMembers.filter(om => !users.some(u => u.Id === om.Id));
            toAdd = users.filter(u => !oldMembers.some(om => u.Id === om.Id));
        }).then(() => this.$q.all(toAdd.map(a =>
            this.$http.put(this.config.ApiBaseUrl + `group/${id}/users`, a).then(() => cb && cb('added', a))
        ))).then(() => this.$q.all(toRemove.map(r =>
            this.$http.delete(this.config.ApiBaseUrl + `group/${id}/users/${r.Id}`).then(() => cb && cb('removed', r))
        ))).then(() => { })
    }

    public GetTeams(): ng.IPromise<OfficeObject[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<OfficeObject[]>(this.config.ApiBaseUrl + 'team')
        ).then(x =>
            x.data);
    }
    public UpdateTeam(datasToUpdate: any): ng.IPromise<OfficeObject> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<OfficeObject>(this.config.ApiBaseUrl + `team/update`, JSON.stringify(datasToUpdate))
        ).then(x =>
            x.data);
    }
    public DeleteTeam(team: OfficeObject): ng.IPromise<void> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.delete<void>(this.config.ApiBaseUrl + `team/${team.Id}`)
        ).then(x => { });
    }
    public PutTeam(data: OfficeObject): ng.IPromise<string> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<string>(this.config.ApiBaseUrl + 'team', JSON.stringify(data))
        ).then(x =>
            x.statusText);
    }
    public GetTeamOwners(id: string): ng.IPromise<User[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User[]>(this.config.ApiBaseUrl + `team/${id}/owners`)
        ).then(x =>
            x.data);
    }
    public UpdateTeamOwners(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void> {
        var toAdd: User[] = [];
        var toRemove: User[] = [];
        users = users || [];
        return this.GetTeamOwners(id).then(oldMembers => {
            toRemove = oldMembers.filter(om => !users.some(u => u.Id === om.Id));
            toAdd = users.filter(u => !oldMembers.some(om => u.Id === om.Id));
        }).then(() => this.$q.all(toAdd.map(a =>
            this.$http.put(this.config.ApiBaseUrl + `team/${id}/owners`, a).then(() => cb && cb('added', a))
        ))).then(() => this.$q.all(toRemove.map(r =>
            this.$http.delete(this.config.ApiBaseUrl + `team/${id}/owners/${r.Id}`).then(() => cb && cb('removed', r))
        ))).then(() => { })
    }
    public GetTeamMembers(id: string): ng.IPromise<User[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User[]>(this.config.ApiBaseUrl + `team/${id}/users`)
        ).then(x =>
            x.data);
    }
    public UpdateTeamMembers(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void> {
        var toAdd: User[] = [];
        var toRemove: User[] = [];
        users = users || [];
        return this.GetTeamMembers(id).then(oldMembers => {
            toRemove = oldMembers.filter(om => !users.some(u => u.Id === om.Id));
            toAdd = users.filter(u => !oldMembers.some(om => u.Id === om.Id));
        }).then(() => this.$q.all(toAdd.map(a =>
            this.$http.put(this.config.ApiBaseUrl + `team/${id}/users`, a).then(() => cb && cb('added', a))
        ))).then(() => this.$q.all(toRemove.map(r =>
            this.$http.delete(this.config.ApiBaseUrl + `team/${id}/users/${r.Id}`).then(() => cb && cb('removed', r))
        ))).then(() => { })
    }

    public GetPlans(): ng.IPromise<OfficeObject[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<OfficeObject[]>(this.config.ApiBaseUrl + 'plan')
        ).then(x =>
            x.data);
    }
    
    public UpdatePlan(datasToUpdate: any): ng.IPromise<OfficeObject> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<OfficeObject>(this.config.ApiBaseUrl + `plan/update`, JSON.stringify(datasToUpdate))
        ).then(x =>
            x.data);
    }
    public DeletePlan(plan: OfficeObject): ng.IPromise<void> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.delete<void>(this.config.ApiBaseUrl + `plan/${plan.Id}`)
        ).then(x => { });
    }
    public PutPlan(data: OfficeObject): ng.IPromise<string> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<string>(this.config.ApiBaseUrl + 'plan', JSON.stringify(data))
        ).then(x =>
            x.statusText);
    }
    public GetPlanOwners(id: string): ng.IPromise<User[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User[]>(this.config.ApiBaseUrl + `plan/${id}/owners`)
        ).then(x =>
            x.data);
    }
    public UpdatePlanOwners(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void> {
        var toAdd: User[] = [];
        var toRemove: User[] = [];
        users = users || [];
        return this.GetPlanOwners(id).then(oldMembers => {
            toRemove = oldMembers.filter(om => !users.some(u => u.Id === om.Id));
            toAdd = users.filter(u => !oldMembers.some(om => u.Id === om.Id));
        }).then(() => this.$q.all(toAdd.map(a =>
            this.$http.put(this.config.ApiBaseUrl + `plan/${id}/owners`, a).then(() => cb && cb('added', a))
        ))).then(() => this.$q.all(toRemove.map(r =>
            this.$http.delete(this.config.ApiBaseUrl + `plan/${id}/owners/${r.Id}`).then(() => cb && cb('removed', r))
        ))).then(() => { })
    }
    public GetPlanMembers(id: string): ng.IPromise<User[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User[]>(this.config.ApiBaseUrl + `plan/${id}/users`)
        ).then(x =>
            x.data);
    }
    public UpdatePlanMembers(id: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void> {
        var toAdd: User[] = [];
        var toRemove: User[] = [];
        users = users || [];
        return this.GetPlanMembers(id).then(oldMembers => {
            toRemove = oldMembers.filter(om => !users.some(u => u.Id === om.Id));
            toAdd = users.filter(u => !oldMembers.some(om => u.Id === om.Id));
        }).then(() => this.$q.all(toAdd.map(a =>
            this.$http.put(this.config.ApiBaseUrl + `plan/${id}/users`, a).then(() => cb && cb('added', a))
        ))).then(() => this.$q.all(toRemove.map(r =>
            this.$http.delete(this.config.ApiBaseUrl + `plan/${id}/users/${r.Id}`).then(() => cb && cb('removed', r))
        ))).then(() => { })
    }

    public GetWorkspaces(): ng.IPromise<OfficeObject[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<OfficeObject[]>(this.config.ApiBaseUrl + 'workspace')
        ).then(x =>
            x.data);
    }

    public GetUnregisteredWorkspaces(): ng.IPromise<OfficeObject[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<OfficeObject[]>(this.config.ApiBaseUrl + 'unregisteredworkspaces')
        ).then(x =>
            x.data);
    }   

    public GetGroupMembersByPage(userPayload: any) {
        return this.auth.acquireToken().then(
            x =>
                this.$http.post<any>(this.config.ApiBaseUrl + `user/group/members`, JSON.stringify(userPayload))
        ).then(x =>
            x.data);
    }

    public GetGroupAllMembers(grpId: string): ng.IPromise<User[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User[]>(this.config.ApiBaseUrl + `user/group/allmembers/${grpId}`, { cache: false })
        ).then(x =>
            x.data);
    }

    public UpdateWorkspace(workspaceId: string, datas:any): ng.IPromise<OfficeObject> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<OfficeObject>(this.config.ApiBaseUrl + `workspace/update/${workspaceId}`, JSON.stringify(datas))
        ).then(x =>
            x.data);
    }

    public DeleteWorkspace(workspace: OfficeObject): ng.IPromise<void> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<OfficeObject>(this.config.ApiBaseUrl + 'workspace/delete', JSON.stringify(workspace))
        ).then(x => { });
    }
    public CreateTeamSite(data: OfficeObject): ng.IPromise<OfficeObject> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<OfficeObject>(this.config.ApiBaseUrl + 'workspace/createteamsite', JSON.stringify(data), { timeout: 600000 })
        ).then(x => x.data);
    }
    public AddWorkspaceOwners(data: OfficeObject): ng.IPromise<OfficeObject> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<OfficeObject>(this.config.ApiBaseUrl + 'workspace/addowners', JSON.stringify(data), { timeout: 600000 })
        ).then(x => x.data);
    }
    public PutWorkspace(data: OfficeObject): ng.IPromise<OfficeObject> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.put<OfficeObject>(this.config.ApiBaseUrl + 'workspace', JSON.stringify(data), { timeout: 600000 })
        ).then(x => x.data);
    }
    public GetWorkspaceOwners(workspaceId: string): ng.IPromise<User[]> {
        return this.auth.acquireToken().then(
            x =>
                this.$http.get<User[]>(this.config.ApiBaseUrl + `workspace/${workspaceId}/owners`)
        ).then(x =>
            x.data);
    }
    public UpdateWorkspaceOwners(workspaceId: string, users: User[], cb?: (a: string, member: User) => void): ng.IPromise<void> {
        var toAdd: User[] = [];
        var toRemove: User[] = [];
        users = users || [];
        return this.GetWorkspaceOwners(workspaceId).then(oldMembers => {
            toRemove = oldMembers.filter(om => !users.some(u => u.Id === om.Id));
            toAdd = users.filter(u => !oldMembers.some(om => u.Id === om.Id));
        }).then(() => this.$q.all(toAdd.map(a =>
            this.$http.put(this.config.ApiBaseUrl + `workspace/${workspaceId}/owners`, a).then(() => cb && cb('added', a))
        ))).then(() => this.$q.all(toRemove.map(r =>
            this.$http.delete(this.config.ApiBaseUrl + `workspace/${workspaceId}/owners/${r.Id}`).then(() => cb && cb('removed', r))
        ))).then(() => { })
    }

    private defaultVal(d: boolean, b: boolean | undefined | null) {
        return b == null ? d : b;
    }
    private defaultTrue(b: boolean | undefined | null) {
        return this.defaultVal(true, b);
    }
    private defaultFalse(b: boolean | undefined | null) {
        return this.defaultVal(false, b);
    }

}

app.service("dataService", DataService);
