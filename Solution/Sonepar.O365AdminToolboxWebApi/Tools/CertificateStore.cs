﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using System.Linq;
using Sonepar.O365AdminToolbox.Domain;

namespace Sonepar.O365AdminToolboxWebApi.Tools
{
    public class CertificateStore : ICertificateStore
    {
        public X509Certificate2 GetCert()
        {
            X509Store certStore = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            certStore.Open(OpenFlags.ReadOnly);
            try
            {
                X509Certificate2Collection certCollection = certStore.Certificates.Find(
                                           X509FindType.FindByThumbprint,
                                            ConfigurationManager.AppSettings["JWT_CERT"],
                                           false);

                if (certCollection.Count > 0)
                {
                    X509Certificate2 cert = certCollection[0];
                    return cert;
                }
                else {
                    var cert = certStore.Certificates.Cast<X509Certificate2>().Where(x => String.Compare(x.Thumbprint, ConfigurationManager.AppSettings["JWT_CERT"], true) == 0).FirstOrDefault();
                    return cert;
                }
            }
            finally
            {
                certStore.Close();
            }
        }
    }
}
