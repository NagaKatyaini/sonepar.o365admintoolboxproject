﻿using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.Extensibility;
using Sonepar.O365AdminToolbox.Domain;
using System;

namespace Sonepar.O365AdminToolboxWebApi
{
    public class LogTelemetryInitializer : ITelemetryInitializer
    {
        public static Func<IAppSettings> AppSettingsFactory; 
        public void Initialize(ITelemetry telemetry)
        {
            IAppSettings settings = null;
            if (AppSettingsFactory != null)
            {
                settings = AppSettingsFactory();
            }
            else
            {
                settings = new AppSettings(new GlobalSettings());
            }
            if (TelemetryConfiguration.Active != null)
            {
                TelemetryConfiguration.Active.InstrumentationKey = settings.AppInsightId;
            }
            telemetry.Context.Properties["App"] = settings.AppInsightId;

        }
    }
}