﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net;

namespace Sonepar.O365AdminToolboxWebApi.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AuthorizeClaimsAadAttribute : AuthorizeClaimsIssuerAttribute
    {
        public AuthorizeClaimsAadAttribute()
        {
            Issuer = $"https://sts.windows.net/{settings.TenantId}/";
        }
    }
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AuthorizeClaimsSelfAttribute : AuthorizeClaimsIssuerAttribute
    {
        public AuthorizeClaimsSelfAttribute() {
            Issuer = settings.Issuer;
        }
    }
    public class AuthorizeClaimsIssuerAttribute : AuthorizationFilterAttribute
    {
        protected static AppSettings settings = new AppSettings(new GlobalSettings());

        protected string Issuer { get; set; }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException(nameof(actionContext));
            }
            var user = actionContext.ControllerContext.RequestContext.Principal as ClaimsPrincipal;
            if (user == null || !user.Claims.Any(x => x.Issuer == Issuer)) {
                HandleUnauthorizedRequest(actionContext);
            }
            base.OnAuthorization(actionContext);
        }

        /// <summary>Processes requests that fail authorization.</summary>
        /// <param name="actionContext">The context.</param>
        protected virtual void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException(nameof(actionContext));
            }
            actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized");
        }

    }
}
