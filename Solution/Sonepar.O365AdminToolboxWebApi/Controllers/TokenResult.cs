﻿namespace Sonepar.O365AdminToolboxWebApi.Controllers
{
    public class TokenResult
    {
        [DataMember]
        public string access_token { get; set; }
        [DataMember]
        public int expires_in { get; set; }
        [DataMember]
        public string token_type { get; set; }
    }
}