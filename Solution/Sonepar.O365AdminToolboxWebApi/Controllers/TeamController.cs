﻿using Sonepar.O365AdminToolbox.Domain;
using System.Collections.Generic;
using Sonepar.O365AdminToolboxWebApi.Authentication;
using System.Threading.Tasks;
using Sonepar.O365AdminToolboxWebApi.Filters;
using System.Web.Http;
using System;
using Sonepar.O365AdminToolbox.Entity;
using System.Text.RegularExpressions;

namespace Sonepar.O365AdminToolboxWebApi.Controllers
{
    /// <summary>
    /// Manage Office 365 Unified teams
    /// </summary>
    /// <seealso cref="BaseController" />
    public class TeamController : BaseController
    {
        private readonly IAppSettings settings;
        private readonly JsonWebTokenFormat format;
        private readonly IUserService userService;
        private readonly ITeamService teamService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamController"/> class.
        /// </summary>
        /// <param name="settings">The app settings.</param>
        /// <param name="format">The JWT formatter.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="teamService">The team service.</param>
        public TeamController(
            IAppSettings settings,
            JsonWebTokenFormat format,
            IUserService userService,
            ITeamService teamService) : base(settings, format, userService)
        {
            this.settings = settings;
            this.format = format;
            this.userService = userService;
            this.teamService = teamService;
        }

        /// <summary>
        /// Gets the list of Office 365 Unified teams.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/team")]
        public async Task<IEnumerable<OfficeObject>> GetTeams()
        {
            return await teamService.GetTeamsManagedByUser(GetUserCountries());
        }

        /// <summary>
        /// Gets a Office 365 Unified team.
        /// </summary>
        /// <param name="teamId">The team identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/team/{teamId}")]
        public async Task<OfficeObject> GetTeams(string teamId)
        {
            return await teamService.GetTeam(teamId, GetUserCountries());
        }

        /// <summary>
        /// Create an Office 365 Unified team.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/team")]
        public async Task<OfficeObject> AddTeam([FromBody]OfficeObject team)
        {
            if (!Regex.IsMatch(team.Name, settings.TeamNameRegex))
                throw new Exception(settings.TeamNameRegexErrorMessage);
            if (!Regex.IsMatch(team.Name, "[a-zA-Z0-9.\\-_]+"))
                throw new Exception("Team name can only contain letters and numbers as well as period, hyphen and underscore characters");

            return await teamService.NewTeam(team, GetUserCountries());
        }

        /// <summary>
        /// Update an Office 365 Team.
        /// </summary>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/team/update")]
        public async Task<OfficeObject> UpdateTeam([FromBody]OfficeObject team)
        {
            if (!Regex.IsMatch(team.Name, settings.TeamNameRegex))
                throw new Exception(settings.TeamNameRegexErrorMessage);

            return await teamService.UpdateTeam(team, base.GetUserCountries());
        }

        /// <summary>
        /// Delete an Office 365 Unified team.
        /// </summary>
        /// <param name="teamId">The team identifier.</param>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/team/{teamId}")]
        public async Task DeleteTeam([FromUri]string teamId)
        {
            await teamService.DeleteTeam(teamId, GetUserCountries());
        }

        /// <summary>
        /// Add an Office 365 Unified team owner.
        /// </summary>
        /// <param name="teamId">The team identifier (Guid-like).</param>
        /// <param name="owner">The owner.</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/team/{teamId}/owners")]
        public async Task AddOwner([FromUri] string teamId, [FromBody] Member owner)
        {
            await teamService.AddTeamOwner(teamId, owner.Id, GetUserCountries());
        }
        /// <summary>
        /// Remove an Office 365 Unified team owner.
        /// </summary>
        /// <param name="teamId">The team identifier (Guid-like).</param>
        /// <param name="ownerId">The owner identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/team/{teamId}/owners/{ownerId}")]
        public async Task DeleteOwner([FromUri] string teamId, [FromUri] string ownerId)
        {
            await teamService.RemoveTeamOwner(teamId, ownerId, GetUserCountries());
        }

        /// <summary>
        /// List the team members.
        /// </summary>
        /// <param name="teamId">The team identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/team/{teamId}/users")]
        public async Task<IEnumerable<Member>> GetTeamMembers(string teamId)
        {
            return await teamService.GetTeamMembers(teamId, GetUserCountries());
        }

        /// <summary>
        /// List the team owners.
        /// </summary>
        /// <param name="teamId">The team identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpGet(), Route("api/team/{teamId}/owners")]
        public async Task<IEnumerable<Member>> GetTeamOwners(string teamId)
        {
            return await teamService.GetTeamOwners(teamId, GetUserCountries());
        }

        /// <summary>
        /// Remove a team member.
        /// </summary>
        /// <param name="teamId">The team identifier (Guid-like).</param>
        /// <param name="userId">The user identifier (Guid-like).</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpDelete(), Route("api/team/{teamId}/users/{userId}")]
        public async Task RemoveTeamMember(string teamId, string userId)
        {
            await teamService.RemoveTeamMember(teamId, userId, GetUserCountries());
        }
        /// <summary>
        /// Add a team member.
        /// </summary>
        /// <param name="teamId">The team identifier (Guid-like).</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        [AuthorizeClaimsSelf(), HttpPut(), Route("api/team/{teamId}/users")]
        public async Task AddTeamMember(string teamId, [FromBody] Member user)
        {
            await teamService.AddTeamMember(teamId, user.Id, GetUserCountries());
        }

    }
}
