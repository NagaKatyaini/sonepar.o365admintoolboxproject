#################################################################################
#
#								Update group
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$group
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Updating Office 365 group"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/group/update" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $group) -ContentType application/json
	Write-Host "Group updated"
}
catch
{
	Write-Error "Unable to update group. Exiting. $($_.Exception.Message)"
	throw
}