#################################################################################
#
#								Add group member
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$groupId,
	[Parameter(Mandatory=$true)]$member
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Adding group member"
	Invoke-Restmethod -method Put -Uri "$endPointTenant/api/group/$groupId/users" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $member) -ContentType application/json
	Write-Host "Group member added"
}
catch
{
	Write-Error "Unable to add group member. Exiting. $($_.Exception.Message)"
	throw
}