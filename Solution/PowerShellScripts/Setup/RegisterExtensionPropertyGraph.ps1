. .\Settings.ps1

$graphtoken=& ".\GetGraphToken.ps1"
$url = "https://graph.microsoft.com/beta/applications/$applicationId/extensionProperties"
$doc=@"
{
    "name": "sonepar_country",
    "dataType": "String",
    "targetObjects": [
        "Group"
    ]
}
"@
$res = Invoke-WebRequest -Method Get -Headers @{ Authorization="bearer $graphtoken" } -Uri $url -ContentType "application/json" | % {$_.Content} | ConvertFrom-Json 
if(-not $res.Value) {
	Invoke-WebRequest -Method Post -Headers @{ Authorization="bearer $graphtoken" } -Body $doc -Uri $url -ContentType "application/json" | out-null
	$res = Invoke-WebRequest -Method Get -Headers @{ Authorization="bearer $graphtoken" } -Uri $url -ContentType "application/json" | % {$_.Content} | ConvertFrom-Json 
}
Write-Host "Name of property: $($res.value.Name)"
$res.value