#Require -Version 5.0
using namespace System.Text;
using namespace System.Security.Cryptography;
using namespace System.Security.Cryptography.X509Certificates;

Param (
	$password,
	$certificateThumprint
)

if(-not $certificateThumprint) {
	$certificateThumprint =  '0C94A56D83E299CB335B50B9DB818D6C970E805E'
}

$certStore = New-Object X509Store -ArgumentList My, CurrentUser
$certStore.Open([OpenFlags]::ReadOnly)

$cert=[X509Certificate2]$certStore.Certificates.Find([X509FindType]::FindByThumbprint, $certificateThumprint, $false)[0]
$rsa=[RSACryptoServiceProvider]$cert.PublicKey.Key
[Convert]::ToBase64String( $rsa.Encrypt([Encoding]::UTF8.GetBytes($password), $false))
