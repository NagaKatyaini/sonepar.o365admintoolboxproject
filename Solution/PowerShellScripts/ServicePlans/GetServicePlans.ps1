#################################################################################
#
#								Get service plans
#
#################################################################################

param(
	[switch] $force
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Getting service plans"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/servicePlan" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}
catch
{
	Write-Error "Unable to get service plans from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}