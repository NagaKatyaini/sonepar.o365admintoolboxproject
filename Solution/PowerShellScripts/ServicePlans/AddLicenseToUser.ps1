#################################################################################
#
#								Add license to user
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$userId,
	[Parameter(Mandatory=$true)]$license
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Adding license to user"
	$p = Invoke-RestMethod -Method Post -Uri "$endPointTenant/api/ServicePlan/$userId/plans" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json -Depth 5 $license) -ContentType application/json 
	Write-Host "License added to user"
}
catch
{
	Write-Error "Unable to add license to user. Exiting. $($_.Exception.Message)"
	throw
}
