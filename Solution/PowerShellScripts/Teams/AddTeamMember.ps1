#################################################################################
#
#								Add team member
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$teamId,
	[Parameter(Mandatory=$true)]$member
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Adding team member"
	Invoke-Restmethod -method Put -Uri "$endPointTenant/api/team/$teamId/users" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $member) -ContentType application/json
	Write-Host "Team member added"
}
catch
{
	Write-Error "Unable to add team member. Exiting. $($_.Exception.Message)"
	throw
}