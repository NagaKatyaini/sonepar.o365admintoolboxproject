#################################################################################
#
#								Add plan owner
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$planId,
	[Parameter(Mandatory=$true)]$owner
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Adding plan owner"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/plan/$planId/owners" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $owner) -ContentType application/json
	Write-Host "Plan owner added"
}
catch
{
	Write-Error "Unable to add plan owner. Exiting. $($_.Exception.Message)"
	throw
}