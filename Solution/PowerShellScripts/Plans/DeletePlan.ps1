#################################################################################
#
#								Delete plan
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$planId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Deleting plan"
	$p = Invoke-RestMethod -Method Delete -Uri "$endPointTenant/api/plan/$planId" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	Write-Host "Plan deleted"
}
catch
{
	Write-Error "Unable to delete plan. Exiting. $($_.Exception.Message)"
	throw
}