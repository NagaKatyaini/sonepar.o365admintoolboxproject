#################################################################################
#
#								Get plan members
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$planId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting plan members"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/plan/$planId/users" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}	
catch
{
	Write-Error "Unable to get plan members from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}