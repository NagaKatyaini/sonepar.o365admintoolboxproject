#################################################################################
#
#								Add plan
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$plan
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Adding plan"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/plan" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $plan) -ContentType application/json
	Write-Host "Plan added"
}
catch
{
	Write-Error "Unable to add plan. Exiting. $($_.Exception.Message)"
	throw
}