#################################################################################
#
#								Add plan member
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$planId,
	[Parameter(Mandatory=$true)]$member
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force

	Write-Host "Adding plan member"
	Invoke-Restmethod -method Put -Uri "$endPointTenant/api/plan/$planId/users" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body (ConvertTo-Json $member) -ContentType application/json
	Write-Host "Plan member added"
}
catch
{
	Write-Error "Unable to add plan member. Exiting. $($_.Exception.Message)"
	throw
}