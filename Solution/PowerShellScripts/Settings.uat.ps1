#################################################################################
#
#								Settings for the project
#
#################################################################################

# Azure Ad Tenant
$aadTenant="soneparbiz.onmicrosoft.com"

# Administration Tool's Endpoint
$endPointTenant="https://o365admintoolboxdev.azurewebsites.net/"
$ApiEndpointUri="https://soneparbiz.onmicrosoft.com/baa69736-bdae-4c91-8ca7-ff7902d8cad2"

# Used to acquire the Token Id for the Admin tools
$redirectUriAdminTools="http://localhost" 
$clientIdAdminTools="c6802608-83b5-4c27-8815-6b633b1c3dc6" # PowerShell (Native) Application ID

#Required for the Aad Token acquisition
$adal = "${env:ProgramFiles(x86)}\WindowsPowerShell\Modules\Azure\5.1.2\Services\Microsoft.IdentityModel.Clients.ActiveDirectory.dll"
$adalforms = "${env:ProgramFiles(x86)}\WindowsPowerShell\Modules\Azure\5.1.2\Services\Microsoft.IdentityModel.Clients.ActiveDirectory.WindowsForms.dll"
$authorityUri = "https://login.windows.net/$aadTenant"

# Change the values for $redirectUri, $clientId and $aadTenant with appropriate values
$redirectUri="https://localhost" 
$clientId="db83b938-62a5-4c9a-9bcc-71a92f4258c6" # WebApi (Native) Application ID

#Graph client settings (i.e. Web Api)
$graphredirectUri="https://localhost" 
$graphClientId="db83b938-62a5-4c9a-9bcc-71a92f4258c6" # WebApi (Native) Application ID
$graphApiEndpointUri="https://graph.microsoft.com"
$applicationId="7516caed-1f69-44c1-8461-ae7fa28d4309" # WebApp (Web App/API) Object ID
