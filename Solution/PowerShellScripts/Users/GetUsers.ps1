#################################################################################
#
#								Get users from Azure AD
#
#################################################################################

param(
	[switch] $force
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	Write-host "$script:apptoken.access_token"
	Write-Debug "Getting user"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/user" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } 
	$r
}
catch
{
	Write-Error "Unable to get users from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}
