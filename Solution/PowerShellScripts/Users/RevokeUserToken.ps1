#################################################################################
#
#								Revoke user token
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$userId
	[Parameter(Mandatory=$true)]$enable
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Updating user status"
	Invoke-RestMethod -Method Put -Uri "$endPointTenant/api/user/$userId/statut" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" } -Body $enabled -ContentType application/json
	Write-Host "User status updated"
}
catch
{
	Write-Error "Unable to update workspace. Exiting. $($_.Exception.Message)"
	throw
}