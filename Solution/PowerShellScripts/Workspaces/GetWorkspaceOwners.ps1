#################################################################################
#
#								Get workspace owners
#
#################################################################################

param(
	[switch] $force,
	[Parameter(Mandatory=$true)]$workspaceId
)

. .\GetAppAuthToken.ps1
. .\Settings.ps1

try
{
	$script:apptoken = getAppToken -Force:$force
	
	Write-Host "Getting workspace owners"
	$r = Invoke-RestMethod -Method Get -Uri "$endPointTenant/api/workspace/$workspaceId/owners" -Headers @{ Authorization="bearer $($script:apptoken.access_token)" }
	$r
}
catch
{
	Write-Error "Unable to get workspaces owners from Azure AD. Exiting. $($_.Exception.Message)"
	throw
}