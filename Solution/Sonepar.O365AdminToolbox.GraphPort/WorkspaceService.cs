﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sonepar.O365AdminToolbox.Domain;
using Sonepar.O365AdminToolbox.Entity;
using System.Data.Entity;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.UserProfiles;
using Microsoft.Online.SharePoint.TenantAdministration;
using Graph = Microsoft.Graph;
using OfficeDevPnP.Core.Sites;
using Microsoft.Graph;
using System.Net;
using System.Text.RegularExpressions;

namespace Sonepar.O365AdminToolbox.GraphPort
{
    public class WorkspaceService : BaseUserService, IWorkspaceService
    {
        private IAdminCredentials adminCredentials;
        private IUserService userService;
        private ToolboxDbContext toolboxContext;
        private IGroupService groupService;

        public WorkspaceService(
            IAuthenticationService helper,
            IAppSettings settings,
            IFriendlyNameService nameService,
            IGroupService groupService,
            IAdminCredentials adminCredentials,
            IUserService userService,
            ToolboxDbContext toolboxContext) : base(helper, settings, nameService)
        {
            this.adminCredentials = adminCredentials;
            this.groupService = groupService;
            this.userService = userService;
            this.toolboxContext = toolboxContext;
        }

        #region IWorkspaceService

        public async Task<IEnumerable<OfficeObject>> GetWorkspacesManagedByUser(IEnumerable<string> countries)
        {
            var workspaces = await toolboxContext.OfficeObjects.Where(t => t.Type == OfficeObjectType.Workspace && countries.Contains(t.Country)).OrderBy(t => t.Country).ThenBy(t => t.Name).ToListAsync();
            foreach(var workspace in workspaces)
            {
                if (string.IsNullOrEmpty(workspace.Url))
                {
                    try
                    {
                        var site = await Client.Sites[workspace.Id].Request().GetAsync();
                        workspace.Url = site.WebUrl;
                        await toolboxContext.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        workspace.Url = "";
                        await toolboxContext.SaveChangesAsync();
                    }
                }
            }
            //if (string.IsNullOrEmpty(workspaces.First().Url))
            //{
            //    workspaces = await CheckAndFixWorkSpacesUrl();
            //}
            return workspaces;
        }

        public async Task<List<OfficeObject>> CheckAndFixWorkSpacesUrl()
        {
            var workspaces = await toolboxContext.OfficeObjects.Where(t => t.Type == OfficeObjectType.Workspace).ToListAsync();
            foreach (var ws in workspaces)
            {
                
            }
            return await toolboxContext.OfficeObjects.Where(t => t.Type == OfficeObjectType.Workspace).ToListAsync();
        }

        public async Task<IEnumerable<OfficeObject>> GetUnregisteredWorkSpaces()
        {
            await this.CheckAndFixWorkSpacesUrl();

            var Sites = new List<OfficeObject>();
            var credentials = adminCredentials.GetAdminCredentials();
            var spCredentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);

            using (var tenantContext = new ClientContext(settings.SharePointRoot) { Credentials = spCredentials })
            {
                var tenant = new Tenant(tenantContext);                
                var sitePropEnumerable = tenant.GetSiteCollections(startIndex: 0, endIndex: 500000, includeDetail: true, includeOD4BSites: true);
                await tenantContext.ExecuteQueryAsync();

                var dbWorkspaces = await toolboxContext.OfficeObjects.Where(t => t.Type == OfficeObjectType.Workspace).ToListAsync();

                foreach (var sp in sitePropEnumerable)
                {
                    int index = dbWorkspaces.FindIndex(f => f.Url == sp.Url);
                    if(index == -1)
                    {
                        var nSite = new OfficeObject()
                        {
                            Name = sp.Title,
                            Url = sp.Url,
                        };
                        Sites.Add(nSite);
                    }                    
                }
            }
            return Sites;
        }

        public async Task<OfficeObject> GetWorkspace(string id, IEnumerable<string> countries)
        {
            var owners = await GetWorkspaceOwners(id, countries);

            var workspace = await toolboxContext.OfficeObjects.SingleAsync(t => t.Id == id && countries.Contains(t.Country));
            workspace.Owners = owners;

            return workspace;
        }

        public async Task<OfficeObject> NewSharePointSite(Workspace workspace)
        {
            var groupExists = await groupService.GroupExists(workspace.Name);
            if (groupExists)
            {
                throw new InvalidOperationException("Group allready exists");
            }
            
            var url = $"{settings.SharePointBaseUrl}/{workspace.Name}";
            var credentials = adminCredentials.GetAdminCredentials();
            var targetTenantUrl = settings.SharePointBaseUrl.Replace("sites", "");

            
            using (var context = new ClientContext(targetTenantUrl))
            {

                var siteExists = WebExistsFullUrl(context, url);
                if (siteExists)
                {
                    throw new InvalidOperationException("Workspace allready exists");
                }

                context.AuthenticationMode = ClientAuthenticationMode.Default;
                context.Credentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);

                bool isPublic = workspace.Visibility == "Private" ? false : true;
                TeamSiteCollectionCreationInformation modernteamSiteInfo = new TeamSiteCollectionCreationInformation
                {
                    Description = workspace.Description,
                    DisplayName = workspace.Name,
                    Alias = workspace.Name,
                    IsPublic = isPublic,
                    Lcid = workspace.Lcid,
                };
                var siteCreation = context.CreateSiteAsync(modernteamSiteInfo);
               
            }
            
            var dbWorkspace = new OfficeObject
            {
                Id = new Guid().ToString(),
                Name = workspace.Name,
                Country = workspace.Country,
                Type = workspace.Type,
                Visibility = workspace.Visibility,
                Url = url
            };
            // Add workspace to database

            return dbWorkspace;
        }
        public async Task<OfficeObject> AddWorkspaceOwners(Workspace workspace, IEnumerable<string> countries)
        {
            // var owerEmailList = new List<string>();
            // var lstOwner = new List<Domain.User>();

            
            using (var transactionScope = toolboxContext.Database.BeginTransaction())
            {
                var credentials = adminCredentials.GetAdminCredentials();
                var spCredentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);

                using (var collectionContext = new ClientContext(workspace.Url) { Credentials = spCredentials })
                {                    

                    var web = collectionContext.Web;
                    var site = collectionContext.Site;
                    var ownersGroup = web.AssociatedOwnerGroup;
                    var regionalSettings = web.RegionalSettings;

                    collectionContext.Load(site, s => s.Id);
                    collectionContext.Load(ownersGroup);
                    collectionContext.Load(web, w => w.RegionalSettings, w => w.RegionalSettings.TimeZones);
                    await collectionContext.ExecuteQueryAsync();

                    var utcTimeZone = web.RegionalSettings.TimeZones.Where(timezone => 
                    timezone.Id == workspace.TimeZoneId).First();
                    web.RegionalSettings.TimeZone = utcTimeZone;

                    // Add owners
                    foreach (var owner in workspace.Owners)
                    {
                        var user = await userService.GetUser(owner.Id, countries);
                        var spUser = web.EnsureUser(user.UserPrincipalName.Split('@')[0]);

                        ownersGroup.Users.AddUser(spUser);
                        ownersGroup.Update();
                    }

                    await collectionContext.ExecuteQueryAsync();

                    workspace.Id = site.Id.ToString();
                    workspace.Type = OfficeObjectType.Workspace;
                }

                var dbWorkspace = new OfficeObject
                {
                    Id = workspace.Id,
                    Name = workspace.Name,
                    Country = workspace.Country,
                    Type = workspace.Type,
                    Visibility = workspace.Visibility,
                    Url = workspace.Url
                };


                toolboxContext.OfficeObjects.Add(dbWorkspace);
                await toolboxContext.SaveChangesAsync();
                transactionScope.Commit();
                return dbWorkspace;
            }
           
        }

        public static bool WebExistsFullUrl(ClientRuntimeContext context, string webFullUrl)
        {
            bool exists = false;
            try
            {
                using (ClientContext testContext = context.Clone(webFullUrl))
                {
                    testContext.Load(testContext.Web, w => w.Title, w => w.Url);
                    testContext.ExecuteQueryRetry();
                    exists = (string.Compare(testContext.Web.Url, webFullUrl, true) == 0);
                }
            }
            catch (Exception ex)
            {
                
            }
            return exists;
        }


        public async Task<OfficeObject> NewWorkspace(Workspace workspace, IEnumerable<string> countries)
        {
            var groupExists = await groupService.GroupExists(workspace.Name);
            if (groupExists)
            {
                throw new InvalidOperationException("Group allready exists");
            }
            using (var transactionScope = toolboxContext.Database.BeginTransaction())
            {
                var url = $"{settings.SharePointBaseUrl}/{workspace.Name}";

                var credentials = adminCredentials.GetAdminCredentials();
                var spCredentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);

                var targetTenantUrl = settings.SharePointBaseUrl.Replace("sites", "");


                var owerEmailList = new List<string>();
                var lstOwner = new List<Domain.User>();

                foreach (var owner in workspace.Owners)
                {
                    var user = await userService.GetUser(owner.Id, countries);
                    lstOwner.Add(user);
                    owerEmailList.Add(user.Mail);
                }

                using (var context = new ClientContext(targetTenantUrl))
                {
                    var siteExists = WebExistsFullUrl(context, url);
                    if (siteExists)
                    {
                        throw new InvalidOperationException("Workspace with this URL allready exists");
                    }

                    context.AuthenticationMode = ClientAuthenticationMode.Default;
                    context.Credentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);

                    bool isPublic = workspace.Visibility == "Private" ? false : true;
                    TeamSiteCollectionCreationInformation modernteamSiteInfo = new TeamSiteCollectionCreationInformation
                    {
                        Description = workspace.Description,
                        DisplayName = workspace.Name,                        
                        Alias = workspace.Name,                        
                        IsPublic = isPublic,
                        Lcid = workspace.Lcid,     
                        Owners = owerEmailList.ToArray()
                    };
                    var createModernSite = context.CreateSiteAsync(modernteamSiteInfo);

                    var IsWorkSpaceCreated = false;
                    while (!IsWorkSpaceCreated)
                    {
                        await Task.Delay(2000);
                        try
                        {
                            IsWorkSpaceCreated = WebExistsFullUrl(context, url);
                            if(createModernSite.Exception != null && !IsWorkSpaceCreated)
                            {
                                throw new InvalidOperationException(createModernSite.Exception.InnerException.Message);
                            }
                        }
                        catch (ServiceException e)
                        {
                            // Not associated member
                            if (e.StatusCode != HttpStatusCode.Forbidden)
                                throw;
                        }
                    }
                }                

                using (var collectionContext = new ClientContext(url) { Credentials = spCredentials })
                {
                    var web = collectionContext.Web;
                    var site = collectionContext.Site;
                    var ownersGroup = web.AssociatedOwnerGroup;
                    var regionalSettings = web.RegionalSettings;

                    collectionContext.Load(site, s => s.Id, s => s.Url);
                    collectionContext.Load(ownersGroup);
                    collectionContext.Load(web, w => w.RegionalSettings, w => w.RegionalSettings.TimeZones);
                    await collectionContext.ExecuteQueryAsync();

                    var utcTimeZone = web.RegionalSettings.TimeZones.Where(timezone =>
                    timezone.Id == workspace.TimeZoneId).First();
                    web.RegionalSettings.TimeZone = utcTimeZone;
                    web.RegionalSettings.Update();
                    // Add owners
                    foreach (var owner in lstOwner)
                    {
                        //var user = await userService.GetUser(owner.Id, countries);
                        var spUser = web.EnsureUser(owner.UserPrincipalName.Split('@')[0]);
                        ownersGroup.Users.AddUser(spUser);
                    }
                    ownersGroup.Update();

                    await collectionContext.ExecuteQueryAsync();
                    workspace.Id = site.Id.ToString();
                    workspace.Type = OfficeObjectType.Workspace;
                }

                var dbWorkspace = new OfficeObject
                {
                    Id = workspace.Id,
                    Name = workspace.Name,
                    Country = workspace.Country,
                    Type = workspace.Type,
                    Visibility = workspace.Visibility,
                    Url = url
                };
                // Add workspace to database
                toolboxContext.OfficeObjects.Add(dbWorkspace);
                await toolboxContext.SaveChangesAsync();
                transactionScope.Commit();

                return dbWorkspace;
            }
        }

        public async Task<OfficeObject> UpdateWorkSpace(string workspaceId, string siteUrl, string newTitle, IEnumerable<string> countries)
        {
            var groupExists = await groupService.GroupExists(newTitle);
            if (groupExists)
            {
                throw new InvalidOperationException("Group allready exists");
            }
            using (var transactionScope = toolboxContext.Database.BeginTransaction())
            {
                //Update Name in DB
                var workspaceDB = toolboxContext.OfficeObjects.Single(t => t.Id == workspaceId);
                workspaceDB.Name = newTitle;
                await toolboxContext.SaveChangesAsync();

                var credentials = adminCredentials.GetAdminCredentials();
                var spCredentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);

                using (var clientContext = new ClientContext(siteUrl) { Credentials = spCredentials })
                {
                    clientContext.Credentials = spCredentials;
                    var web = clientContext.Web;
                    clientContext.Load(web);
                    clientContext.ExecuteQuery();
                    web.Title = newTitle;
                    
                    web.Update();

                    clientContext.Load(clientContext.Site, s => s.RelatedGroupId);
                    clientContext.ExecuteQuery();
                    
                    var workspaceUpdate = new Graph.Group
                    {
                        DisplayName = newTitle,
                        MailNickname = Regex.Replace(newTitle, "[^a-zA-Z0-9.\\-_]", String.Empty)
                    };

                    if (clientContext.Site.RelatedGroupId != Guid.Empty)
                    {
                        var groupId = clientContext.Site.RelatedGroupId.ToString();
                        await Client.Groups[groupId].Request().UpdateAsync(workspaceUpdate);
                    }
                    
                    await clientContext.ExecuteQueryAsync();
                }

                transactionScope.Commit();
                return workspaceDB;
            }
        }

        public async Task DeleteWorkspace(string url, IEnumerable<string> countries)
        {
            using (var transactionScope = toolboxContext.Database.BeginTransaction())
            {
                // Remove workspace from database
                var workspace = toolboxContext.OfficeObjects.Single(t => t.Url == url);
                toolboxContext.OfficeObjects.Remove(workspace);
                await toolboxContext.SaveChangesAsync();

                // Remove workspace (SharePoint)
                // var url = $"{settings.SharePointBaseUrl}/{workspace.Name}";
                var credentials = adminCredentials.GetAdminCredentials();
                var spCredentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);


                using (var clientContext = new ClientContext(url) { Credentials = spCredentials })
                {
                    try
                    {
                        clientContext.Load(clientContext.Site, s => s.RelatedGroupId);
                        await clientContext.ExecuteQueryAsync();

                    }
                    catch (Exception ex)
                    {
                        //if (ex.StatusCode != HttpStatusCode.NotFound)
                        //{
                            transactionScope.Commit();
                            throw new Exception("Site does not exist and has been removed from the toolbox", ex);
                        //}                            
                    }
                    

                    if (clientContext.Site.RelatedGroupId != Guid.Empty)
                    {
                        var isWorkSpaceSiteDeleted = false;
                        while (!isWorkSpaceSiteDeleted)
                        {                            
                            try
                            {
                                await Client.Groups[clientContext.Site.RelatedGroupId.ToString()].Request().DeleteAsync();
                            }
                            catch (ServiceException ex)
                            {
                                if (ex.StatusCode == HttpStatusCode.NotFound)
                                    isWorkSpaceSiteDeleted = true;
                            }
                        }                        
                    }

                    using (var rootContext = new ClientContext(settings.SharePointRoot) { Credentials = spCredentials })
                    {
                        var tenant = new Tenant(rootContext);
                        tenant.RemoveSite(workspace.Url);
                        rootContext.Load(tenant);
                        var isWorkSpaceSiteDeleted = false;
                        while (!isWorkSpaceSiteDeleted)
                        {
                            await Task.Delay(1000);
                            try
                            {
                                await rootContext.ExecuteQueryAsync();
                                isWorkSpaceSiteDeleted = true;
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                                                                       
                    }
                    transactionScope.Commit();
                }                
            }
        }

        public async Task<IEnumerable<Member>> GetWorkspaceOwners(string id, IEnumerable<string> countries)
        {
            var users = new List<Member>();

            var workspace = toolboxContext.OfficeObjects.Single(t => t.Id == id);

            // var fullUrl = $"{settings.SharePointBaseUrl}/{workspace.Name}";
            var wsUrl = workspace.Url;
            if (string.IsNullOrEmpty(wsUrl))
            {
                var site = await Client.Sites[workspace.Id].Request().GetAsync();
                wsUrl = site.WebUrl;
            }
            var credentials = adminCredentials.GetAdminCredentials();
            var spCredentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);
            using (var context = new ClientContext(wsUrl) { Credentials = spCredentials })
            {
                var web = context.Web;
                var ownersGroup = web.AssociatedOwnerGroup;

                context.Load(ownersGroup, g => g.Users);
                await context.ExecuteQueryAsync();

                foreach (var spUser in ownersGroup.Users)
                {
                    var manager = new PeopleManager(context);
                    var properties = manager.GetPropertiesFor(spUser.LoginName);

                    context.Load(properties);
                    await context.ExecuteQueryAsync();

                    if (properties.ServerObjectIsNull.HasValue && !properties.ServerObjectIsNull.Value)
                    {
                        var user = new Domain.User
                        {
                            Id = properties.UserProfileProperties["msOnline-ObjectId"],
                            DisplayName = properties.UserProfileProperties["PreferredName"]
                        };

                        users.Add(user);
                    }
                }
            }

            return users.AsEnumerable();
        }

        public async Task AddWorkspaceOwner(string id, string userId, IEnumerable<string> countries)
        {
            var workspace = await toolboxContext.OfficeObjects.SingleAsync(t => t.Id == id);
            var user = await userService.GetUser(userId, countries);

            var fullUrl = $"{settings.SharePointBaseUrl}/{workspace.Name}";
            var credentials = adminCredentials.GetAdminCredentials();
            var spCredentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);

            using (var context = new ClientContext(fullUrl) { Credentials = spCredentials })
            {
                var web = context.Web;
                var ownersGroup = web.AssociatedOwnerGroup;

                var spUser = web.EnsureUser(user.UserPrincipalName.Split('@')[0]);
                ownersGroup.Users.AddUser(spUser);

                await context.ExecuteQueryAsync();
            }
        }

        public async Task RemoveWorkspaceOwner(string id, string userId, IEnumerable<string> countries)
        {
            var workspace = await toolboxContext.OfficeObjects.SingleAsync(t => t.Id == id);
            var user = await userService.GetUser(userId, countries);

            var fullUrl = $"{settings.SharePointBaseUrl}/{workspace.Name}";
            var credentials = adminCredentials.GetAdminCredentials();
            var spCredentials = new SharePointOnlineCredentials(credentials.UserLogin, credentials.Password);

            using (var context = new ClientContext(fullUrl) { Credentials = spCredentials })
            {
                var web = context.Web;
                var ownersGroup = web.AssociatedOwnerGroup;

                var spUser = web.EnsureUser(user.UserPrincipalName.Split('@')[0]);
                ownersGroup.Users.Remove(spUser);

                await context.ExecuteQueryAsync();
            }
        }

        #endregion
    }
}