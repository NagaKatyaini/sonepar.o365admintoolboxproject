﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sonepar.O365AdminToolbox.Domain;
using Sonepar.O365AdminToolbox.Entity;
using System.Data.Entity;
using Microsoft.Graph;
using System.Net;
using System.Text.RegularExpressions;
using System;
using Graph = Microsoft.Graph;

namespace Sonepar.O365AdminToolbox.GraphPort
{
    public class PlanService : BaseUserService, IPlanService
    {
        private IGroupService groupService;
        private ToolboxDbContext toolboxContext;

        public PlanService(
            IAuthenticationService helper,
            IAppSettings settings,
            IFriendlyNameService nameService,
            IGroupService groupService,
            ToolboxDbContext toolboxContext) : base(helper, settings, nameService)
        {
            this.groupService = groupService;
            this.toolboxContext = toolboxContext;
        }

        #region IPlanService

        public async Task<IEnumerable<OfficeObject>> GetPlansManagedByUser(IEnumerable<string> countries)
        {
            var plans = await toolboxContext.OfficeObjects.Where(t => t.Type == OfficeObjectType.Plan && countries.Contains(t.Country)).OrderBy(t => t.Country).ThenBy(t => t.Name).ToListAsync();

            return plans;
        }

        public async Task<OfficeObject> GetPlanAsync(string id, IEnumerable<string> countries)
        {
            var group = await groupService.GetGroup(id, countries);
            var plan = groupService.MapOfficeObject(group);

            return plan;
        }

        public async Task<OfficeObject> NewPlan(OfficeObject plan, IEnumerable<string> countries)
        {
            using (var transactionScope = toolboxContext.Database.BeginTransaction())                
            {
                var groupExists = await groupService.GroupExists(plan.Name);
                if (groupExists)
                {
                    throw new InvalidOperationException("Group allready exists");
                }
                // Add plan (group) to Azure AD
                var group = groupService.MapGroup(plan);
                group.MailNickname = Regex.Replace(group.MailNickname, "[^a-zA-Z0-9.\\-_]", String.Empty);
                group = await groupService.NewGroup(group, countries);

                // Owners member assocation (prerequisite)
                foreach (var owner in plan.Owners)
                    await groupService.AddGroupMember(group.Id, owner.Id, countries);

                // Admin member association (prerequisite)
                var adminId = settings.AdminId;
                var member = await Client.DirectoryObjects[adminId].Request().GetAsync();
                await Client.Groups[group.Id].Members.References.Request().AddAsync(member);

                // Plan creation retry process
                var plannerPlan = new PlannerPlan { Owner = group.Id, Title = group.Name };
                var IsPlanCreated = false;
                while (!IsPlanCreated)
                {
                    await Task.Delay(1000);
                    try
                    {
                        plannerPlan = await Client.Planner.Plans.Request().AddAsync(plannerPlan);
                        IsPlanCreated = true;
                    }
                    catch (ServiceException e)
                    {
                        // Not associated member
                        if (e.StatusCode != HttpStatusCode.Forbidden)
                            throw;
                    }
                }

                // Admin member dissociation
                await Client.Groups[group.Id].Members[adminId].Reference.Request().DeleteAsync();

                // Add plan to database
                plan.Id = group.Id;
                plan.Type = OfficeObjectType.Plan;

                toolboxContext.OfficeObjects.Add(plan);
                await toolboxContext.SaveChangesAsync();

                transactionScope.Commit();

                return plan;
            }
        }


        public async Task<OfficeObject> UpdatePlan(OfficeObject plan, IEnumerable<string> countries)
        {
            using (var transactionScope = toolboxContext.Database.BeginTransaction())
            {
                var groupExists = await groupService.GroupExists(plan.Name);
                if (groupExists)
                {
                    throw new InvalidOperationException("Group allready exists");
                }
                var planUpdate = new Graph.Group
                {
                    DisplayName = plan.Name,
                    MailNickname = Regex.Replace(plan.Name, "[^a-zA-Z0-9.\\-_]", String.Empty)
                };
                await Client.Groups[plan.Id]
                .Request()
                .UpdateAsync(planUpdate);

                var planDB = toolboxContext.OfficeObjects.Single(t => t.Id == plan.Id);
                planDB.Name = plan.Name;
                await toolboxContext.SaveChangesAsync();


                var group = await groupService.GetGroup(plan.Id, countries);
                var planUpdated = groupService.MapOfficeObject(group);

                transactionScope.Commit();

                return planUpdated;
            }
        }

        public async Task DeletePlan(string id, IEnumerable<string> countries)
        {
            using (var transactionScope = toolboxContext.Database.BeginTransaction())
            {
                // Remove plan from database
                var plan = toolboxContext.OfficeObjects.Single(t => t.Id == id);
                toolboxContext.OfficeObjects.Remove(plan);
                await toolboxContext.SaveChangesAsync();

                // Remove plan (group) from Azure AD
                await groupService.DeleteGroup(id, countries);

                transactionScope.Commit();
            }
        }

        public async Task<IEnumerable<Member>> GetPlanOwners(string id, IEnumerable<string> countries)
        {
            return await groupService.GetGroupOwners(id, countries);
        }

        public async Task AddPlanOwner(string id, string userId, IEnumerable<string> countries)
        {
            await groupService.AddGroupOwner(id, userId, countries);

            var existingGroupMembers = await groupService.GetGroupMembers(id, countries);
            if (!existingGroupMembers.Any(t => t.Id == userId))
                await groupService.AddGroupMember(id, userId, countries);
        }

        public async Task RemovePlanOwner(string id, string userId, IEnumerable<string> countries)
        {
            await groupService.RemoveGroupOwner(id, userId, countries);
        }

        public async Task<IEnumerable<Member>> GetPlanMembers(string id, IEnumerable<string> countries)
        {
            return await groupService.GetGroupMembers(id, countries);
        }

        public async Task AddPlanMember(string id, string userId, IEnumerable<string> countries)
        {
            await groupService.AddGroupMember(id, userId, countries);
        }

        public async Task RemovePlanMember(string id, string userId, IEnumerable<string> countries)
        {
            await groupService.RemoveGroupMember(id, userId, countries);
        }

        #endregion
    }
}
