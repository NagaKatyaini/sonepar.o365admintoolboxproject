﻿using System;
using Graph = Microsoft.Graph;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Sonepar.O365AdminToolbox.Domain;

namespace Sonepar.O365AdminToolbox.GraphPort
{
    public class BaseUserService 
    {
        private const int UserPageSize = 999;
        private const int maxFilterCount = 10;
        protected const string AdminOfClaimType = "https://governance.sonepar.com/claims/adminOf";
        protected IAppSettings settings;
        protected readonly IAuthenticationService helper;
        private static readonly Regex re = new Regex("^(.*)$");
        protected readonly IFriendlyNameService nameService;

        private static string[] UserProperties = new String[] { "id","accountEnabled","assignedLicenses","assignedPlans","businessPhones","city","companyName","country","department","displayName","givenName","jobTitle","mail","mailNickname","mobilePhone","onPremisesImmutableId","onPremisesLastSyncDateTime","onPremisesSecurityIdentifier","onPremisesSyncEnabled","passwordPolicies","passwordProfile","officeLocation","postalCode","preferredLanguage","provisionedPlans","proxyAddresses","state","streetAddress","surname","usageLocation","userPrincipalName","userType" };

        static BaseUserService()
        {
            //UserProperties = typeof(Microsoft.Graph.User)
            //    .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy)
            //    .Where(x => !x.PropertyType.Name.EndsWith("Page"))
            //    .Select(x => x.Name).ToArray();
        }

        private Graph.GraphServiceClient clientInstance = null;
        protected Graph.GraphServiceClient Client
        {
            get
            {
                if (clientInstance == null)
                {
                    clientInstance = helper.GetGraphClientAsAdmin();
                }
                return clientInstance;
            }
        }


        public BaseUserService(
            IAuthenticationService helper, 
            IAppSettings settings, 
            IFriendlyNameService nameService
            ) {
            this.settings = settings;
            this.helper = helper;
            this.nameService = nameService;     
        }

        protected Task<Graph.User> GetGraphUserMin(string Id, IEnumerable<string> inCountries)
        {
            return GetGraphUser(Id, inCountries, null);
        }
        protected Task<Graph.User> GetGraphUser(string Id, IEnumerable<string> inCountries) {
            return GetGraphUser(Id,inCountries, UserProperties);
        }
        protected async Task<Graph.User> GetGraphUser(string Id, IEnumerable<string> inCountries, IEnumerable<string> properties)
        {
            var userReq = Client.Users[Id].Request();
            if (properties != null)
            {
                userReq = userReq.Select(String.Join(",", properties));
            }
            var user = await userReq.GetAsync();
            var memberOfReq = await Client.Users[Id].MemberOf.Request().Top(999).GetAsync();
            var memberOf = memberOfReq.CurrentPage.ToList();
            var groups = GetCountryGroupNames(inCountries);

            while (memberOfReq.NextPageRequest != null)
            {
                memberOfReq = await memberOfReq.NextPageRequest.GetAsync();
                memberOf.AddRange(memberOfReq.CurrentPage.ToList());
            }

            if (!memberOf.OfType<Graph.Group>().Any(m => groups.Any(g => re.Replace(g,"$1").ToLower() == m.DisplayName.ToLower())))
            {
                throw new InvalidOperationException("User Not Found");
            }

            return user;
        }

        protected IEnumerable<string> GetCountryGroupNames(IEnumerable<string> countries)
        {
            return countries.Select(GetCountryGroupName);
        }
        protected string GetCountryGroupName(string country)
        {
            return re.Replace(country, settings.ManagedGroupsReplace);
        }
        public async Task<IEnumerable<License>> GetServicePlansAvailable()
        {
            var user = await Client.Education.Me.User.Request().GetAsync();
            var currentUserPreferredLanguage = !string.IsNullOrEmpty(user.PreferredLanguage) ? user.PreferredLanguage : "en-US";

            var plansQuery = await Client.SubscribedSkus.Request().GetAsync();
            var plans = plansQuery.CurrentPage.ToList();
            while (plansQuery.NextPageRequest != null)
            {
                plansQuery = await plansQuery.NextPageRequest.GetAsync();
                plans.AddRange(plansQuery.CurrentPage.ToList());
            }

            var result = plans.Select(plan => new License {
                FriendlyName = nameService.GetNameOrDefault(plan.SkuPartNumber, plan.SkuPartNumber, currentUserPreferredLanguage),
                Name = plan.SkuPartNumber,
                Id = plan.SkuId,
                AppliesTo = plan.AppliesTo,
                ConsumedUnits = plan.ConsumedUnits,
                PrepaidUnitsEnabled = plan.PrepaidUnits?.Enabled,
                Services = plan.ServicePlans.Select(p => new ServiceAssignement {
                    Service = new Service
                    {
                        FriendlyName = nameService.GetNameOrDefault(p.ServicePlanName, p.ServicePlanName, currentUserPreferredLanguage),
                        Name = p.ServicePlanName,
                        Id = p.ServicePlanId.Value,
                        AppliesTo = p.AppliesTo
                    },
                    IsActive = true
                })
            });
            return result;

        }

        //public void RegisterNames(IEnumerable<License> plans) {
        //    foreach (var plan in plans)
        //    {
        //        nameService.Register(registeredName: plan.FriendlyName, serviceName: plan.Id.ToString(), Type: ServiceType.Plan);
        //    }
        //}
        protected User MapUser(Graph.User x)
        {
            return MapUser(x, plans: null);
        }

        protected User MapUser(Graph.User user, IEnumerable<License> plans)
        {
            IList<License> plansAssigned = null;
            if (plans != null)
            {
                plansAssigned = user.AssignedLicenses?
                    .Where(l => plans.Any(p => p.Id == l.SkuId.Value))
                    .Select(l => {
                    var copy = plans.First(p => p.Id == l.SkuId.Value).Copy();
                    copy.Services.ToList().ForEach(s => s.IsActive = !l.DisabledPlans.Contains(s.Service.Id));
                    return copy;
                }).ToList();
            }
            var services = (plans ?? Enumerable.Empty<License>()).SelectMany(x => x.Services).GroupBy(x => x.Service.Id).Select(x => x.First().Service).ToDictionary(x => x.Id);

            var assignedServices = user.AssignedPlans?.Select(y => new Service()
            {
                Name = y.Service,
                AppliesTo = y.ServicePlanId.HasValue && services.ContainsKey(y.ServicePlanId.Value) ? services[y.ServicePlanId.Value].AppliesTo : null,
                FriendlyName = nameService.GetNameOrDefault(y.Service, y.Service, "en-US"),
                Id = y.ServicePlanId.Value
            })?.ToList();

            return new User
            {
                AccountEnabled = user.AccountEnabled,
                AssignedLicenses = plansAssigned,
                AssignedPlans = assignedServices,
                City = user.City,
                CompanyName = user.CompanyName,
                Country = user.Country,
                Department = user.Department,
                OnPremisesSyncEnabled = user.OnPremisesSyncEnabled,
                DisplayName = user.DisplayName,
                BusinessPhones = user.BusinessPhones,
                GivenName = user.GivenName,
                Id = user.Id,
                JobTitle = user.JobTitle,
                OnPremisesLastSyncDateTime = user.OnPremisesLastSyncDateTime,
                Mail = user.Mail,
                MailNickname = user.MailNickname,
                MobilePhone = user.MobilePhone,
                OnPremisesSecurityIdentifier = user.OnPremisesSecurityIdentifier,
                PasswordPolicies = user.PasswordPolicies,
                OfficeLocation = user.OfficeLocation,
                PostalCode = user.PostalCode,
                PreferredLanguage = user.PreferredLanguage,
                State = user.State,
                StreetAddress = user.StreetAddress,
                Surname = user.Surname,
                UsageLocation = user.UsageLocation,
                UserPrincipalName = user.UserPrincipalName,
                UserType = user.UserType,
            };
        }

    }
}
