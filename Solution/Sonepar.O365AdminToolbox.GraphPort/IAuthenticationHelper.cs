﻿using System.Threading.Tasks;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Graph;
using Sonepar.O365AdminToolbox.Domain;

namespace Sonepar.O365AdminToolbox.GraphPort
{
    public interface IAuthenticationService
    {
        Task<string> AcquireTokenAsyncForAdmin(string resource);
        ActiveDirectoryClient GetActiveDirectoryClientAsAdmin();
        GraphServiceClient GetGraphClientAsAdmin();
        GraphServiceClient GetGraphClientAsUser(UserPassCredentials user);
    }
}